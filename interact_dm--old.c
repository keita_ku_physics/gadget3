#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>
#include "allvars.h"
#include "proto.h"

/*! \file interact_dm.c 
 *  \brief routines for self-interacting dark matter, e.g., evaporation
 * HERE: mass-conversions just increase or decrease particle velocity by a certain amount; no random kicks
 */


#ifdef EVAPORATING_DM

/* ------------------------------------- */
/* --- this stuff is needed to make nearest_ngb to work --- */	
#ifdef PERIODIC
static double boxSize, boxHalf;

#ifdef LONG_X
static double boxSize_X, boxHalf_X;
#else
#define boxSize_X boxSize
#define boxHalf_X boxHalf
#endif
#ifdef LONG_Y
static double boxSize_Y, boxHalf_Y;
#else
#define boxSize_Y boxSize
#define boxHalf_Y boxHalf
#endif
#ifdef LONG_Z
static double boxSize_Z, boxHalf_Z;
#else
#define boxSize_Z boxSize
#define boxHalf_Z boxHalf
#endif
#endif
/* --- end this stuff --- */
/* ------------------------------------- */



/*! This function computes self-interaction probabilities and performs Monte Carlo on all particles.
 *	If a random number is greater than the probability of interaction, the particle is assigned a
 *	randon kick velocity and changes its type (heavy->light) for the EVAPORATING_DM model.
 */
void interact_dm(void)
{
	int n,i,j,ip,jp,k,chan,process;
	double dtime, a_beg, a_end, a3, rand1, rand2, chance, rho, pvel, v, vconv, vconv_internal, vrand[3], vnorm;
	double Time_to_s, Tbegin_in_s, Densities_to_g_per_cm3, velfac, sigma_fi;
	double v1[3],v2[3],v1_2,v2_2,V_CentMass[3],m1,m2,delE,tot2E,pi,pf;
	int *temp;
	long long Count[9];
	int count[9]={0,0,0,0,0,0,0,0,0},type1,type2,type3[4],type4[4],m_changes,pr1,pr2;
	double prob[4],prob_tot;
	double m3[4],m4[4],v3[4],v4[4];
//	double V_CM;


/* ------------------------------------- */
/* --- this stuff is needed to make nearest_ngb to work --- */	
#ifdef PERIODIC
	boxSize = All.BoxSize;
	boxHalf = 0.5 * All.BoxSize;
#ifdef LONG_X
	boxHalf_X = boxHalf * LONG_X;
	boxSize_X = boxSize * LONG_X;
#endif
#ifdef LONG_Y
	boxHalf_Y = boxHalf * LONG_Y;
	boxSize_Y = boxSize * LONG_Y;
#endif
#ifdef LONG_Z
	boxHalf_Z = boxHalf * LONG_Z;
	boxSize_Z = boxSize * LONG_Z;
#endif
#endif
/* --- end this stuff --- */	
/* ------------------------------------- */
	
	if (ThisTask == 0) {
		printf("\nBegin dark matter self-interactions...\n");
		fflush(stdout);
	}
	
	
	/* initialize NearestNgb variables */
	for(n = 0; n < NumPart; n++) {
		SphP[n].NearestNgb = -1;				/* has no pair */
		SphP[n].NearestNgbSep = All.BoxSize;	/* something large */
	}
	
	
	Time_to_s = All.UnitTime_in_s / All.Hubble/All.HubbleParam;
	Tbegin_in_s = All.TimeBegin * Time_to_s;
	
	Densities_to_g_per_cm3 = All.UnitMass_in_g / (All.UnitLength_in_cm * All.UnitLength_in_cm * All.UnitLength_in_cm);
	
	vconv = All.VelConv * 1e5;	/* velocity kick due to conversion in cm/s */

	
	for(i = 0; i < NumPart; i++)
	{
		
//		dtime = (P[i].Ti_endstep - P[i].Ti_begstep) * All.Timebase_interval; /* = Delta(Ln(a)), i.e.,
//																			  * difference in log of the expansion factor */
		
		a_beg = All.TimeBegin * exp(P[i].Ti_begstep * All.Timebase_interval); /* scale factor at the beginning of timestep */
		a_end = All.TimeBegin * exp(P[i].Ti_endstep * All.Timebase_interval); /* scale factor at the end of timestep */
		dtime = (a_end - a_beg) * Time_to_s; /* time step in seconds */
		a3 = a_beg * a_beg * a_beg;
		velfac = 1.0 / a_beg;			/* = sqrt(a_beg)/sqrt(a3)  -- conversion factors a_beg are collected here 
										 * per Volker's io.c and manual (cosmological simulations) */

		/* get energy change, deltaE=m_l*v_conv^2/2, in internal units */
		vconv_internal = vconv / (velfac * All.UnitVelocity_in_cm_per_s); // convert velocity to internal comoving units
		delE = 0.5 * All.MassTable[TYPE_L] * vconv_internal * vconv_internal; 

		j = nearest_ngb(i);		// j=nearest neighbour; if j<0, no pair is found within Hsml

		if (j < 0)
        {			// no neighbours within Hsml; proceed to the next particle
			count[5]++;		// count particles for which neighbors have no pair to interact with
			continue;
		}
		
		
/* ----- Scrambling particle order for Monte-Carlo ----- */
		/* pick a particle in the pair to be the first and the other to be the second */
		chance = get_random_number(P[i].ID); // a random number
		if (0.5 < chance)
        {					// 50/50 chance
			ip = i;
			jp = j;
		}
        else
        {
			ip = j;
			jp = i;
		}
/* ----- ----- */
		
		m1 = P[ip].Mass;
		m2 = P[jp].Mass;
		type1 = P[ip].Type;
		type2 = P[jp].Type;
		for (k = 0; k < 3; k++)
        {
			v1[k] = P[ip].Vel[k];									// lab frame velocities
			v2[k] = P[jp].Vel[k];
			V_CentMass[k] = (m1 * v1[k] + m2 * v2[k])/(m1 + m2);	// velocity of the Center of Mass
			v1[k] -= V_CentMass[k];									// go to the Center of Mass frame 
			v2[k] -= V_CentMass[k];
		}
		
		/* getting peculiar particles' Relative Velocity in CM frame, in cm/s */
		pvel = (v2[0] - v1[0]) * (v2[0] - v1[0]) + (v2[1] - v1[1]) * (v2[1] - v1[1]) + (v2[2] - v1[2]) * (v2[2] - v1[2]);
		pvel = sqrt(pvel);									// in internal units 
		v = pvel * velfac * All.UnitVelocity_in_cm_per_s;	/* from internal units to cm/s */
		
		v1_2 = v1[0] * v1[0] + v1[1] * v1[1] + v1[2] * v1[2]; 
		v2_2 = v2[0] * v2[0] + v2[1] * v2[1] + v2[2] * v2[2];
		
		pi = m1 * sqrt(v1_2); /* initial momentum of each particle; equal in CM-frame */
		
		/* -- Consider four reaction channels -- */
		for (chan = 0; chan < 4; chan++)
        {
			switch (chan)
            {
				case 0:	/* ..-> HH */
					type3[chan] = TYPE_H;
					type4[chan] = TYPE_H;
					break;
				case 1:	/* ..-> HL */
					type3[chan] = TYPE_H;
					type4[chan] = TYPE_L;
					break;
				case 2:	/* ..-> LH */
					type3[chan] = TYPE_L;
					type4[chan] = TYPE_H;
					break;
				case 3:	/* ..-> LL */
					type3[chan] = TYPE_L;
					type4[chan] = TYPE_L;
					break;				
				default:
					printf("\nError with reaction channels. We terminate execution!\n\n"); // should never happen
					fflush(stdout);
					endrun(1);
					break;
			}

			m3[chan] = All.MassTable[type3[chan]];		 
			m4[chan] = All.MassTable[type4[chan]];
			m_changes = type3[chan] + type4[chan] - type1 - type2;	/* counts # h->l; if negative: l->h */
			tot2E = m1 * v1_2 + m2 * v2_2 + 2.0 * delE * (double)m_changes;
														
			if (tot2E < 0.0) // kinematically forbidden
            {
				prob[chan] = 0.0;
			}
            else
            {
				v3[chan] = sqrt( tot2E / (m3[chan] + m3[chan] * m3[chan] / m4[chan]) );	// vels of particles 3 & 4 (outgoing)
				v4[chan] = - v3[chan] * m3[chan] / m4[chan];
			
				pf = m3[chan] * v3[chan];	// final momentum of a particle; they are the same for each particle in CM-frame 
				/* i->f process cross-section for a (non-)elastic prosess */ 
				pr1 = 2 * (type1 - 1) + (type2 - 1);				// incoming particle combination
				pr2 = 2 * (type3[chan] - 1) + (type4[chan] - 1);	// outgoing particle combination
				sigma_fi = All.MatrixVfi2[pr1][pr2] * All.SigmaTotal * (pf / pi); /* |Vfi|^2 are normalized matrix elements
																				   * SigmaTotal=Vaa^2+Vab^2+Vba^2+Vbb^2 */
				
				/* mean density of each species in gramm/cm^3; NB conversion factor from comoving to physical: 1/a^3 */
				rho = 0.5 * (SphP[ip].PartialDens[type2] + SphP[jp].PartialDens[type2]) / a3 * Densities_to_g_per_cm3; 
				prob[chan] = sigma_fi * rho * v * dtime;	// collision probability
			} 
		}
		
		prob_tot = prob[0] + prob[1] + prob[2] + prob[3];	// total probability of all processes
		
		if (prob_tot < -0.000001)
        {
			printf("\nWarning! Probability of all processes is negative! prob_tot = %g\n\n",prob_tot); 
			fflush(stdout);
			continue;
		}
		
		/* MONTE-CARLO */
		
	/* ---- get random velocity unit vector ---- */
		vrand[0] = get_random_number(P[i].ID + 1) - 0.5; // here "+k+1..3" is to avoid correlations 
		vrand[1] = get_random_number(P[i].ID + 2) - 0.5;
		vrand[2] = get_random_number(P[i].ID + 3) - 0.5;
		vnorm = sqrt(vrand[0] * vrand[0] + vrand[1] * vrand[1] + vrand[2] * vrand[2]); //magnitude
		vrand[0] /= vnorm;		// normalized random velocity
		vrand[1] /= vnorm;
		vrand[2] /= vnorm;
	/* ----------------------------------------- */
		
		/* get random numbers */
		rand1 = get_random_number(P[i].ID - 1); // here -1 and -2 are to avoid correlations 
		rand2 = get_random_number(P[i].ID - 2); 

		if (prob_tot > rand1)
        {
			count[0]++;		// count all interactions
			process = -1;
			/* which process occurs? */
			if (rand2 < prob[0] / prob_tot)
            {
				process = 0;
				count[1]++;		// count ..->HH 
			}
            else if (prob[0] / prob_tot <= rand2 && rand2 < (prob[0] + prob[1]) / prob_tot)
            {
				process = 1;
				count[2]++;		// count ..->HL 			
			}
            else if ((prob[0] + prob[1]) / prob_tot <= rand2 && rand2 < (prob[0] + prob[1] + prob[2]) / prob_tot)
            {
				process = 2;
				count[3]++;		// count ..->LH 			
			}
            else if ((prob[0] + prob[1] + prob[2]) / prob_tot <= rand2)
            {
				process = 3;
				count[4]++;		// count ..->LL 
			}
            else
            {
				printf("\nError within Monte-Carlo module. We terminate execution! \n\n"); 
				fflush(stdout);
				endrun(1);
			}
			
			if (process < 0) // should never happen
            {
				printf("\nError within Monte-Carlo module. We terminate execution! \n\n"); 
				fflush(stdout);
				endrun(1);
			}
			
			/* writing results */
			P[ip].Mass = m3[process];					// assign new masses
			P[jp].Mass = m4[process];
			for (k = 0; k < 3; k++)
            {
				P[ip].Vel[k] = vrand[k] * v3[process];	// assign new random velocities in the CM frame
				P[jp].Vel[k] = vrand[k] * v4[process];
				P[ip].Vel[k] += V_CentMass[k];			// go back to the lab frame
				P[jp].Vel[k] += V_CentMass[k]; 
			}
			P[ip].Type = type3[process];				// change type of particle
			P[jp].Type = type4[process];
			
			
//			V_CM = V_CentMass[0] * V_CentMass[0] + V_CentMass[1] * V_CentMass[1] + V_CentMass[2] * V_CentMass[2];
//			printf("v1=%g, V_CM=%g, v3=%g, process=%d\n",sqrt(v1_2), sqrt(V_CM), v3[process], process);
			
			/* counting */
			m_changes = type3[process] + type4[process] - type1 - type2;	/* counts # h->l; if negative: l->h */
			if (m_changes > 0)
            {
				count[6] += m_changes;	// count all H->L conversions (down-conversions)
                /* add to conversions counter for each particle in the pair */
                SphP[ip].Nconv++;
                SphP[jp].Nconv++;
			}
            else if (m_changes < 0)
            {
				count[7] -= m_changes;	// count all L->H conversions (up-conversions)
                /* add to conversions counter for each particle in the pair */
                SphP[ip].Nconv++;
                SphP[jp].Nconv++;
			}
            else
            {
				count[8]++;				// count all scatterings H->H and L->L (including HL->LH and LH->HL)
                /* add to scatterings counter for each particle in the pair */
                SphP[ip].Nscat++;
                SphP[jp].Nscat++;
			}
			
		}
	}
		

	
/* ---- sum all counters ---- */
	/* because Count[] is of type `long long', we cannot do a simple
	 * MPI_Allreduce() to sum the total particle numbers 
	 */
	temp = malloc(NTask * 9 * sizeof(int));
	MPI_Allgather(count, 9, MPI_INT, temp, 9, MPI_INT, MPI_COMM_WORLD);
	for(i = 0; i < 9; i++)
	{
		Count[i] = 0;
		for(j = 0; j < NTask; j++)
			Count[i] += temp[j * 9 + i];
	}
	free(temp);
/* --------------------------- */	
	
	if (ThisTask == 0) {
		printf("NO PAIR to interact with exists for %lld particles",Count[5]);
		printf("\nINTERACTIONS occurred for %lld pairs",Count[0]);
		printf("\nCONVERSION #s: ..->HH for %lld pairs, ..->LL for %lld pairs",Count[1],Count[4]);
		printf("\nCONVERSION #s: ..->HL for %lld pairs, ..->LH for %lld pairs",Count[2],Count[3]);
		printf("\nTOTALS: H->L for %lld part., L->H for %lld part., scatt. for %lld part.\n",Count[6],Count[7],Count[8]);
		printf("Dark matter self-interactions done.\n\n");
		fflush(stdout);
	}
//	printf("\ntimebegin(billion yr)=%g, timemax(billion yr)=%g\n\n",Tbegin_in_s/3600/24/365/1e9,Tend_in_s/3600/24/365/1e9);
}



/*! This function is a cut-off version of density_evaluate and hydro_evaluate, it finds the nearest neighbor of the target.
 *  The target particle may either be local, or reside in the communication buffer.
 */
int nearest_ngb(int target)
{
	int j, n, startnode, numngb, numngb_inbox;
	double h, hj, dx, dy, dz, r, r2;
	FLOAT *pos;

	int nearest_ngb = -1;		// no heighbor
	double r_ngb = All.BoxSize;	// a large value

	
	pos = P[target].Pos;
	h = SphP[target].Hsml;	
	
	startnode = All.MaxPart;
	numngb = 0;
	do
    {
		numngb_inbox = ngb_treefind_variable(&pos[0], h, &startnode);	// used in density_evaluate
//		numngb_inbox = ngb_treefind_pairs(&pos[0], h, &startnode);		// used in hydro_evaluate
		
		for(n = 0; n < numngb_inbox; n++)
		{
			j = Ngblist[n];
			
			dx = pos[0] - P[j].Pos[0];
			dy = pos[1] - P[j].Pos[1];
			dz = pos[2] - P[j].Pos[2];
			
#ifdef PERIODIC			/*  now find the closest image in the given box size  */
			if(dx > boxHalf_X)
				dx -= boxSize_X;
			if(dx < -boxHalf_X)
				dx += boxSize_X;
			if(dy > boxHalf_Y)
				dy -= boxSize_Y;
			if(dy < -boxHalf_Y)
				dy += boxSize_Y;
			if(dz > boxHalf_Z)
				dz -= boxSize_Z;
			if(dz < -boxHalf_Z)
				dz += boxSize_Z;
#endif
			r2 = dx * dx + dy * dy + dz * dz;
			r = sqrt(r2);	
			hj = SphP[target].Hsml;
			
			if (r < h || r < hj) {		// inside the largest Hsml, otherwise the particle has no pair to interact
			numngb++;
	/* search block that finds the nearest neighbour */
				if (SphP[target].NearestNgb < 0 && SphP[j].NearestNgb < 0)	// if these particles have no pairs 
					if (r < r_ngb && r > 0.000001) {					// make sure it's closer but not the same particle
						nearest_ngb = j;
						r_ngb = r;
//						printf("target = %d; nearest_ngb = %d, r_ngb = %g\n",target,nearest_ngb, r_ngb);
					}
			}
		}
    }
	while(startnode >= 0);
	
	
	
/* record the nearest neighbour pair */
	SphP[target].NearestNgb = nearest_ngb;
	SphP[nearest_ngb].NearestNgb = target;
	SphP[target].NearestNgbSep = SphP[nearest_ngb].NearestNgbSep = r_ngb;
	
	return(nearest_ngb);
	
}

#endif

