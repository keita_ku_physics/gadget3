#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <gsl/gsl_interp.h>

#include "allvars.h"
#include "proto.h"

#include "cooling.h"

/*
 * This is includes some functions for metal cooling by JHC
 * If METAL_COOLING is not defined, this routine is not effective
 */
 
#ifdef  METAL_COOLING

#include "metalcooling.h"

/* The METALCOOLSD table should be rectangular gride */
#define NumFeH  8  /* The first data is for zero metalicity */
#define NumLogT 91
static float  FeH_SD[NumFeH];
static float  LogT_SD[NumLogT];
static float  Lambda_SD[NumFeH][NumLogT];
static float  Ne_SD[NumFeH][NumLogT];


static double ZMin = 1.0E-5;    /* Minimum metallicity to invoke matalcooling under METAL_COOLING 
				 * Too small ZMin cause too much extrapolation.
				 */
static double Lambda_eta = 1.0E-70;

void read_metalcool_table_SD(char *filename)
{
  int i, j;
  FILE *fmetalcool;

  if(!(fmetalcool = fopen(filename,"r")))
    {
      printf("Cannot read metal cooling table in file %s \n",filename);
      endrun(15);
    }

  for(i = 0; i < NumFeH; i++){
    for(j=0; j < NumLogT; j++) {
      fscanf(fmetalcool,"%g %g %g %g",&FeH_SD[i], &LogT_SD[j], &Ne_SD[i][j], &Lambda_SD[i][j]);
    }}

  fclose(fmetalcool);
}


double CoolingRate_from_SD(double logT, double Z)
{
  int nFeH, nLogT;
  double FeH, Lambda, Lambda0, Lambda1, Lambda2, Lambda_excess;

  FeH = Z_to_FeH(Z);

  /* Since the nFeH = 0 is zero metalicity data*/
  nFeH=1;
  while ((FeH_SD[nFeH] <= FeH) && (nFeH < NumFeH-1)) nFeH++;
  if(nFeH == 1) nFeH++;
  nLogT=0;
  while ((LogT_SD[nLogT] <= logT) && (nLogT < NumLogT-1)) nLogT++;
  if(nLogT == 0) nLogT++;

  /*
   * Temperature interpolation first and do FeH interlocation
   */

  Lambda1 = (Lambda_SD[nFeH-1][nLogT-1] * (logT - LogT_SD[nLogT])-
	     Lambda_SD[nFeH-1][nLogT] * (logT - LogT_SD[nLogT-1]))
    /(LogT_SD[nLogT-1] - LogT_SD[nLogT]);
  Lambda2 = (Lambda_SD[nFeH][nLogT-1] * (logT - LogT_SD[nLogT])-
	     Lambda_SD[nFeH][nLogT] * (logT - LogT_SD[nLogT-1]))
    /(LogT_SD[nLogT-1] - LogT_SD[nLogT]);
  Lambda = (Lambda1 * (FeH-FeH_SD[nFeH])-
	    Lambda2 * (FeH-FeH_SD[nFeH-1]))
    /(FeH_SD[nFeH-1] - FeH_SD[nFeH]);

  /* Primodial value*/
  Lambda0 = (Lambda_SD[0][nLogT-1] * (logT - LogT_SD[nLogT])-
	     Lambda_SD[0][nLogT] * (logT - LogT_SD[nLogT-1]))
    /(LogT_SD[nLogT-1] - LogT_SD[nLogT]);

  Lambda_excess = pow(10.0, Lambda) - pow(10.0, Lambda0);
  if(Lambda_excess < Lambda_eta) Lambda_excess = Lambda_eta; 

  
  return log10(Lambda_excess);
}


// Total electron density with normalize N_H=1
double Ne_from_SD(double logT, double Z)
{
  int nFeH, nLogT;
  double FeH, Ne, Ne1, Ne2, Ne0, Ne_excess;

  FeH = Z_to_FeH(Z);

  nFeH=1;
  while ((FeH_SD[nFeH] <= FeH) && (nFeH < NumFeH-1)) nFeH++;
  if(nFeH == 1) nFeH++;
  nLogT=0;
  while ((LogT_SD[nLogT] <= logT) && (nLogT < NumLogT-1)) nLogT++;
  if(nLogT == 0) nLogT++;
  
  Ne1 = (Ne_SD[nFeH-1][nLogT-1] * (logT - LogT_SD[nLogT])-
	 Ne_SD[nFeH-1][nLogT] * (logT - LogT_SD[nLogT-1]))
    /(LogT_SD[nLogT-1] - LogT_SD[nLogT]);
  Ne2 = (Ne_SD[nFeH][nLogT-1] * (logT - LogT_SD[nLogT])-
	 Ne_SD[nFeH][nLogT] * (logT - LogT_SD[nLogT-1]))
    /(LogT_SD[nLogT-1] - LogT_SD[nLogT]);
  Ne = (Ne1*(FeH-FeH_SD[nFeH])-
	Ne2*(FeH-FeH_SD[nFeH-1]))
    /(FeH_SD[nFeH-1] - FeH_SD[nFeH]);
  
  Ne0 = (Ne_SD[0][nLogT-1] * (logT - LogT_SD[nLogT])-
	Ne_SD[0][nLogT] * (logT - LogT_SD[nLogT-1]))
    /(LogT_SD[nLogT-1] - LogT_SD[nLogT]);

  Ne_excess = Ne - Ne0;

  if(Ne_excess < 0.0) Ne_excess = 0.0;

  return Ne_excess;
}


double Z_to_FeH(double Z)
{
  double X_solar=0.7;
  double Z_solar=0.02;
  double fact, Y, FeH;

  if(Z < ZMin) Z = ZMin;      /* floor at ZMin */

  fact = Mz_from_Z(Z_solar) * X_solar / Z_solar;
  Y = Y_from_Z(Z);

  FeH = log10(fact * Z/((1.0 - Y - Z)*Mz_from_Z(Z)));
  return FeH;
}

double Y_from_Z(double Z)
{
  /*
   * If Z <= Z1 ([Fe/H] <= -1), the gas has promodial mixture,
   * if Z >= Z2 ([Fe/H] >=  0), the solar mixture,
   * and if Z1 < Z < Z2, interpolated value
   */
  double Z1 = 0.0018;
  double Z2 = 0.02;
  double Y1 = 0.24;
  double Y2 = 0.28;
  double log_Y, val;

  if(Z <= Z1)
    val = 0.24;
  else 
    {
      if ( Z >= Z2) 
	val = 0.28;
      else 
	{
	  log_Y = (log10(Y1) * (log10(Z) - log10(Z2))-
		   log10(Y2) * (log10(Z) - log10(Z1)))
	    /(log10(Z1) - log10(Z2));
	  val = pow(10.0,log_Y);
	}
    }
 
  return val;
}

double Mz_from_Z(double Z)
{
  return 17.0;
}


#endif

