/* last modified: March 22, 2013

 */
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <math.h>
#include <gsl/gsl_math.h>

#include "allvars.h"
#include "proto.h"

#if defined(STELLAR_FEEDBACK) || defined(SNII)

/*! Structure for communication during the density computation. Holds data that is sent to other processors.
 */

static struct sniidensdata_in
{
    MyDouble   Pos[3];
    MyFloat    Hsml;
    
    int NodeList[NODELISTLENGTH];
}
*SNIIDensDataIn, *SNIIDensDataGet;


static struct sniidensdata_out
{
    MyLongDouble Rho;
    MyLongDouble DhsmlDensity;
    MyLongDouble Ngb;
}
*SNIIDensDataResult, *SNIIDensDataOut;


/*! Does almost the same calulcation as density.c, 
 * but including star particles and even wind particles.
 */
void snii_density(void)
{
    MyFloat *Left, *Right;
    int i, j, ndone, ndone_flag, npleft, dummy, iter = 0;
    int ngrp, sendTask, recvTask, place, nexport, nimport;
    long long ntot;
    double dmax1, dmax2, fac;
    double desnumngb;
   
    
#ifdef  USE_ISEND_IRECV
    MPI_Status stat[2];
    MPI_Request mpireq[2];
#endif
    
    Left = (MyFloat *) mymalloc(NumPart * sizeof(MyFloat));
    Right = (MyFloat *) mymalloc(NumPart * sizeof(MyFloat));
    
    for(i = 0; i < NumPart; i++)
    {
       
        if(P[i].Type == 4 && P[i].Exploded == 0)
            Left[i] = Right[i] = 0;
        else
            Left[i] = Right[i] = -1;
    }
    
    /* allocate buffers to arrange communication */
    
    
    Ngblist = (int *) mymalloc(NumPart * sizeof(int));
    
    All.BunchSize =
    (int) ((All.BufferSize * 1024 * 1024) / (sizeof(struct data_index) + sizeof(struct data_nodelist) +
                                             sizeof(struct sniidensdata_in) + sizeof(struct sniidensdata_out) +
                                             sizemax(sizeof(struct sniidensdata_in),
                                                     sizeof(struct sniidensdata_out))));
    DataIndexTable = (struct data_index *) mymalloc(All.BunchSize * sizeof(struct data_index));
    DataNodeList = (struct data_nodelist *) mymalloc(All.BunchSize * sizeof(struct data_nodelist));
    
    desnumngb = All.DesNumNgb;
    
    /* we will repeat the whole thing for those particles where we didn't find enough neighbours */
    do
    {
        do
        {
            for(j = 0; j < NTask; j++)
            {
                Send_count[j] = 0;
                Exportflag[j] = -1;
            }
            
            /* do local particles and prepare export list */
            for(i = 0, nexport = 0; i < NumPart; i++)
            {
                if(P[i].Type == 4 && P[i].Exploded == 0 && Left[i] >= 0)
                {
                    if(snii_density_evaluate(i, 0, &nexport, Send_count) < 0)
                        break;
                }
            }
            
#ifdef MYSORT
            mysort_dataindex(DataIndexTable, nexport, sizeof(struct data_index), data_index_compare);
#else
            qsort(DataIndexTable, nexport, sizeof(struct data_index), data_index_compare);
#endif
            
            MPI_Allgather(Send_count, NTask, MPI_INT, Sendcount_matrix, NTask, MPI_INT, MPI_COMM_WORLD);
            
            for(j = 0, nimport = 0, Recv_offset[0] = 0, Send_offset[0] = 0; j < NTask; j++)
            {
                Recv_count[j] = Sendcount_matrix[j * NTask + ThisTask];
                nimport += Recv_count[j];
                
                if(j > 0)
                {
                    Send_offset[j] = Send_offset[j - 1] + Send_count[j - 1];
                    Recv_offset[j] = Recv_offset[j - 1] + Recv_count[j - 1];
                }
            }
            
            SNIIDensDataGet = (struct sniidensdata_in *) mymalloc(nimport * sizeof(struct sniidensdata_in));
            SNIIDensDataIn = (struct sniidensdata_in *) mymalloc(nexport * sizeof(struct sniidensdata_in));
            
            /* prepare particle data for export */
            for(j = 0; j < nexport; j++)
            {
                place = DataIndexTable[j].Index;
                
                SNIIDensDataIn[j].Pos[0] = P[place].Pos[0];
                SNIIDensDataIn[j].Pos[1] = P[place].Pos[1];
                SNIIDensDataIn[j].Pos[2] = P[place].Pos[2];
                SNIIDensDataIn[j].Hsml   = P[place].sniiHsml;
                
                memcpy(SNIIDensDataIn[j].NodeList,
                       DataNodeList[DataIndexTable[j].IndexGet].NodeList, NODELISTLENGTH * sizeof(int));
            
            }
            
            /* exchange particle data */
            for(ngrp = 1; ngrp < (1 << PTask); ngrp++)
            {
                sendTask = ThisTask;
                recvTask = ThisTask ^ ngrp;
                
                if(recvTask < NTask)
                {
                    if(Send_count[recvTask] > 0 || Recv_count[recvTask] > 0)
                    {
                        /* get the particles */
                        
#ifdef USE_ISEND_IRECV
                        MPI_Isend(&SNIIDensDataIn[Send_offset[recvTask]],
                                  Send_count[recvTask] * sizeof(struct sniidensdata_in), MPI_BYTE,
                                  recvTask, TAG_DENS_A,MPI_COMM_WORLD, &mpireq[0]);
                        MPI_Irecv(&SNIIDensDataGet[Recv_offset[recvTask]],
                                  Recv_count[recvTask] * sizeof(struct sniidensdata_in), MPI_BYTE,
                                  recvTask, TAG_DENS_A, MPI_COMM_WORLD, &mpireq[1]);
                        MPI_Waitall(2,mpireq,stat);
#else
                        MPI_Sendrecv(&SNIIDensDataIn[Send_offset[recvTask]],
                                     Send_count[recvTask] * sizeof(struct sniidensdata_in), MPI_BYTE,
                                     recvTask, TAG_DENS_A,
                                     &SNIIDensDataGet[Recv_offset[recvTask]],
                                     Recv_count[recvTask] * sizeof(struct sniidensdata_in), MPI_BYTE,
                                     recvTask, TAG_DENS_A, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
#endif
                    }
                }
            }
            
            myfree(SNIIDensDataIn);
            SNIIDensDataResult = (struct sniidensdata_out *) mymalloc(nimport * sizeof(struct sniidensdata_out));
            SNIIDensDataOut = (struct sniidensdata_out *) mymalloc(nexport * sizeof(struct sniidensdata_out));
            
            /* now do the particles that were sent to us */
            for(j = 0; j < nimport; j++)
                snii_density_evaluate(j, 1, &dummy, &dummy);
        
            
            if(i >= NumPart)
                ndone_flag = 1;
            else
                ndone_flag = 0;
            
            MPI_Allreduce(&ndone_flag, &ndone, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
            
            /* get the result */
            for(ngrp = 1; ngrp < (1 << PTask); ngrp++)
            {
                sendTask = ThisTask;
                recvTask = ThisTask ^ ngrp;
                if(recvTask < NTask)
                {
                    if(Send_count[recvTask] > 0 || Recv_count[recvTask] > 0)
                    {
                        /* send the results */
#ifdef USE_ISEND_IRECV
                        MPI_Isend(&SNIIDensDataResult[Recv_offset[recvTask]],
                                  Recv_count[recvTask] * sizeof(struct sniidensdata_out), MPI_BYTE,
                                  recvTask, TAG_DENS_B,MPI_COMM_WORLD, &mpireq[0]);
                        MPI_Irecv(&SNIIDensDataOut[Send_offset[recvTask]],
                                  Send_count[recvTask] * sizeof(struct sniidensdata_out), MPI_BYTE,
                                  recvTask, TAG_DENS_B, MPI_COMM_WORLD, &mpireq[1]);
                        MPI_Waitall(2,mpireq,stat);
#else
                        MPI_Sendrecv(&SNIIDensDataResult[Recv_offset[recvTask]],
                                     Recv_count[recvTask] * sizeof(struct sniidensdata_out),
                                     MPI_BYTE, recvTask, TAG_DENS_B,
                                     &SNIIDensDataOut[Send_offset[recvTask]],
                                     Send_count[recvTask] * sizeof(struct sniidensdata_out),
                                     MPI_BYTE, recvTask, TAG_DENS_B, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
#endif
                    }
                }
                
            }
            
            /* add the result to the local particles */
            for(j = 0; j < nexport; j++)
            {
                place = DataIndexTable[j].Index;
                
                P[place].sniin.dNumNgb += SNIIDensDataOut[j].Ngb;
                
                if(P[place].Type == 4 && P[place].Exploded == 0)
                {
                    P[place].sniid.dDensity += SNIIDensDataOut[j].Rho;
                    P[place].sniih.dDhsmlDensityFactor += SNIIDensDataOut[j].DhsmlDensity;
                    
                }
            }
            
            myfree(SNIIDensDataOut);
            myfree(SNIIDensDataResult);
            myfree(SNIIDensDataGet);
        }
        while(ndone < NTask);
    
        
        //////// The following is the main difference from hydro_force() because now numngb_inbox has to be ////////
        //////// reduced to a sphere, which ngbnum returned from ngb_treefind_pairs() does not need doing.  ////////
        
        
        for(i = 0, npleft = 0; i < NumPart; i++)
        {
            if(P[i].Type == 4 && P[i].Exploded == 0 && Left[i] >= 0)
            {
                if(P[i].sniid.Density > 0)
                {
                    P[i].sniih.DhsmlDensityFactor *= P[i].sniiHsml / (NUMDIMS * P[i].sniid.Density);
                    if(P[i].sniih.DhsmlDensityFactor > -0.9)   /* note: this would be -1 if only a single particle at zero lag is found */
                        P[i].sniih.DhsmlDensityFactor = 1 / (1 + P[i].sniih.DhsmlDensityFactor);
                    else
                        P[i].sniih.DhsmlDensityFactor = 1;
                }
            
                /* now check whether we had enough neighbours */
                if(P[i].sniin.NumNgb < (desnumngb - All.MaxNumNgbDeviation) ||
                   P[i].sniin.NumNgb > (desnumngb + All.MaxNumNgbDeviation))
                {
                    /* need to redo this particle */
                    npleft++;
                    
                    if(Left[i] > 0 && Right[i] > 0)
                        if((Right[i] - Left[i]) < 1.0e-3 * Left[i])
                        {
                            /* this one should be ok */
                            npleft--;
                            Left[i] = -1;           /* Mark as inactive */
                            continue;
                        }
                    
                    if(P[i].sniin.NumNgb < (desnumngb - All.MaxNumNgbDeviation))
                        Left[i] = DMAX(P[i].sniiHsml, Left[i]);
                    else
                    {
                        if(Right[i] != 0)
                        {
                            if(P[i].sniiHsml < Right[i])
                                Right[i] = P[i].sniiHsml;
                        }
                        else
                            Right[i] = P[i].sniiHsml;
                    }
                    
                    if(iter >= MAXITER - 10)
                    {
                        printf
                        ("i=%d task=%d ID=%d Hsml=%g Left=%g Right=%g Ngbs=%g Right-Left=%g\n   pos=(%g|%g|%g)\n",
                         i, ThisTask, (int) P[i].ID, P[i].sniiHsml, Left[i], Right[i],
                         (float) P[i].sniin.NumNgb, Right[i] - Left[i], P[i].Pos[0], P[i].Pos[1], P[i].Pos[2]);
                        fflush(stdout);
                    }
                    
                    if(Right[i] > 0 && Left[i] > 0)
                        P[i].sniiHsml = pow(0.5 * (pow(Left[i], 3) + pow(Right[i], 3)), 1.0 / 3);
                    else
                    {
                        if(Right[i] == 0 && Left[i] == 0)
                            endrun(8188);	/* can't occur */
                        
                        if(Right[i] == 0 && Left[i] > 0)
                        {
                            if(P[i].Type == 4 && P[i].Exploded == 0 && fabs(P[i].sniin.NumNgb - desnumngb) < 0.5 * desnumngb)
                            {
                                fac = 1 - (P[i].sniin.NumNgb -
                                           desnumngb) / (NUMDIMS * P[i].sniin.NumNgb) *
                                P[i].sniih.DhsmlDensityFactor;
                                
                                if(fac < 1.26)
                                    P[i].sniiHsml *= fac;
                                else
                                    P[i].sniiHsml *= 1.26;
                            }
                            else
                                P[i].sniiHsml *= 1.26;
                        }
                        
                        if(Right[i] > 0 && Left[i] == 0)
                        {
                            if(P[i].Type == 4 && P[i].Exploded == 0 && fabs(P[i].sniin.NumNgb - desnumngb) < 0.5 * desnumngb)
                            {
                                fac = 1 - (P[i].sniin.NumNgb -
                                           desnumngb) / (NUMDIMS * P[i].sniin.NumNgb) *
                                P[i].sniih.DhsmlDensityFactor;
                                
                                if(fac > 1 / 1.26)
                                    P[i].sniiHsml *= fac;
                                else
                                    P[i].sniiHsml /= 1.26;
                            }
                            else
                                P[i].sniiHsml /= 1.26;
                        }
                    }
                }
                else
                    Left[i] = -1;  /* Mark as inactive */

            }

        }
        
        sumup_large_ints(1, &npleft, &ntot);
        
        if(ntot > 0)
        {
            iter++;
            
            if(iter > MAXITER)
            {
                printf("failed to converge in neighbour iteration in snii_density()\n");
                fflush(stdout);
                endrun(1155);
            }
        }
        
    }
    while(ntot > 0);
    
    myfree(DataNodeList);
    myfree(DataIndexTable);
    myfree(Ngblist);
    myfree(Right);
    myfree(Left);
    
}



int snii_density_evaluate(int target, int mode, int *nexport, int *nsend_local)
{
    int i, j, n;
    int startnode, numngb, numngb_inbox, listindex = 0;
    double h, h2, hinv, hinv3, hinv4;
    MyLongDouble rho;
    double wk, dwk;
    double dx, dy, dz, r, r2, u, mass_j;
    MyLongDouble weighted_numngb;
    MyLongDouble dhsmlrho;
    
    MyDouble *pos;
    rho = weighted_numngb = dhsmlrho = 0;
    
    
    if(mode == 0)
    {
        pos         = P[target].Pos;
        h           = P[target].sniiHsml;
    }
    else
    {
        pos         = SNIIDensDataGet[target].Pos;
        h           = SNIIDensDataGet[target].Hsml;
    }
    
    
    h2 = h * h;
    hinv = 1.0 / h;
#ifndef  TWODIMS
    hinv3 = hinv * hinv * hinv;
#else
    hinv3 = hinv * hinv / boxSize_Z;
#endif
    hinv4 = hinv3 * hinv;
    
    
    if(mode == 0)
    {
        startnode = All.MaxPart;	/* root node */
    }
    else
    {
        startnode = SNIIDensDataGet[target].NodeList[0];
        startnode = Nodes[startnode].u.d.nextnode;	/* open it */
    }
    
    numngb = 0;
    
    while(startnode >= 0)
    {
        while(startnode >= 0)
        {
            numngb_inbox = ngb_treefind_variable(pos, h, target, &startnode, mode, nexport, nsend_local);
            
            if(numngb_inbox < 0)
                return -1;
                
           
              for(n = 0; n < numngb_inbox; n++)
            {
                j = Ngblist[n];
                
                dx = pos[0] - P[j].Pos[0];
                dy = pos[1] - P[j].Pos[1];
                dz = pos[2] - P[j].Pos[2];
                
#ifdef PERIODIC			/*  now find the closest image in the given box size  */
                if(dx > boxHalf_X)
                    dx -= boxSize_X;
                if(dx < -boxHalf_X)
                    dx += boxSize_X;
                if(dy > boxHalf_Y)
                    dy -= boxSize_Y;
                if(dy < -boxHalf_Y)
                    dy += boxSize_Y;
                if(dz > boxHalf_Z)
                    dz -= boxSize_Z;
                if(dz < -boxHalf_Z)
                    dz += boxSize_Z;
#endif
                r2 = dx * dx + dy * dy + dz * dz;
                
                
                if(r2 < h2)
                {
                    numngb++;
                    
                    r = sqrt(r2);
                    u = r * hinv;
                    
                    if(u < 0.5)
                    {
                        wk = hinv3 * (KERNEL_COEFF_1 + KERNEL_COEFF_2 * (u - 1) * u * u);
                        dwk = hinv4 * u * (KERNEL_COEFF_3 * u - KERNEL_COEFF_4);
                    }
                    else
                    {
                        wk = hinv3 * KERNEL_COEFF_5 * (1.0 - u) * (1.0 - u) * (1.0 - u);
                        dwk = hinv4 * KERNEL_COEFF_6 * (1.0 - u) * (1.0 - u);
                    }
                    
                    mass_j = P[j].Mass;
                    
                    rho += FLT(mass_j * wk);
                    weighted_numngb += FLT(NORM_COEFF * wk / hinv3);	
                    dhsmlrho += FLT(-mass_j * (NUMDIMS * hinv * wk + u * dwk));
        
                }
            }
        
        }
        
        if(mode == 1)
        {
            listindex++;
            if(listindex < NODELISTLENGTH)
            {
                startnode = SNIIDensDataGet[target].NodeList[listindex];
                if(startnode >= 0)
                    startnode = Nodes[startnode].u.d.nextnode;	/* open it */
            }
        }
    }
    
    if(mode == 0)
    {
        P[target].sniin.dNumNgb = weighted_numngb;
        P[target].sniid.dDensity = rho;
        P[target].sniih.dDhsmlDensityFactor = dhsmlrho;
    }
    else
    {
        SNIIDensDataResult[target].Rho = rho;
        SNIIDensDataResult[target].Ngb = weighted_numngb;
        SNIIDensDataResult[target].DhsmlDensity = dhsmlrho;
    }
    
    
    return 0;
}

void snii_set_hsml_guess(void)
{
    int n, m, no;
    float hsml;
    
    for(n = 0; n < NumPart; n++)
    {
        if(P[n].Type == 4 && P[n].Exploded == 0)
        {
            no = Father[n];
            
            while(no >= 0)
            {
                if(Nodes[no].u.d.mass > All.DesNumNgb * P[n].Mass)
                    break;
                no = Nodes[no].u.d.father;
            }
            
            hsml = P[n].sniiHsml;
            
            if(no >= 0)
            {
                if(Nodes[no].u.d.mass > All.DesNumNgb * P[n].Mass)
                    hsml = pow(3/(4*M_PI) * All.DesNumNgb * P[n].Mass / Nodes[no].u.d.mass, 1.0/3) * Nodes[no].len;
            }
            
            if(hsml < P[n].sniiHsml)
                P[n].sniiHsml = hsml;
            
            if(P[n].sniiHsml < All.MinGasHsml)
                P[n].sniiHsml = All.MinGasHsml;
        }
    }
}


#endif



