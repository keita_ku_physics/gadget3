#define  MAX_NGB               100000
#define  DesMinSize            32
#define  DeltaNextGroupfinder  0.1
#define  FirstGroupingTime     0.075
#define  MinimumGasDenCut      0.01
#define  PhysicalSFDensity     1
#define  MergerLengthFactor    1.0

void groupfinder_properties(void);

void find_subgroups(void);
void sort2_flt_int(unsigned long long n, float arr[], int brr[]);
int compare_head_index(const void *a, const void *b);
int compare_dens_index(const void *a, const void *b);
int compare_ghostdens_index(const void *a, const void *b);

void baryondensity(void);
int baryondensity_evaluate(int target, int mode, int *nexport, int *nsend_local);
void set_hsml_guess(void);

#ifdef SIZE_OF_GALAXY_CEILING
int ghost_in(float max_search_local);
#else
int ghost_in(void);
#endif
void ghost_out(int nexport);

int ngb_treefind_baryon(MyDouble searchcenter[3], MyFloat hsml, int target, int *startnode, int mode,
			int *nexport, int *nsend_local);
int ngb_treefind_groupfind(MyDouble searchcenter[3], MyFloat hsml, int target, int *startnode, int mode);


