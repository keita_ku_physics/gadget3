/* last modified: Mar 22, 2014
 
 */
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <math.h>

#include "allvars.h"
#include "proto.h"


#if defined(STELLAR_FEEDBACK) || defined(SNII)

void stellar_and_snii_feedback(void)
{

    snii_set_hsml_guess();
    snii_density();
    snii_main();
    return;  
}

#ifdef SNII
void snii_kinetic_feedback(void)
{
    double v_shock, Cs, v_mom;                /* Velocity   */
    double snii_energy, sniik_energy;        /* Energy     */
    double mass;                            /* Mass       */
    double time, t_fade, t_STend, dt;      /* Time       */
    double norm, dir[3];                  /* Direction  */
    double Ngb_Den;                      /* Density    */
    double metallicity;                 /* Z          */
    int i, k;
    double time_explosion;
    double hubble_a, ascale;
    
    if(All.ComovingIntegrationOn)
    {
        hubble_a = hubble_function(All.Time);
        ascale = All.Time;
        time = a2t(All.Time);
    }
    else
    {
        hubble_a = ascale = 1;
        time = All.Time;
    }
    
    for(i = FirstActiveParticle; i >= 0; i = NextActiveParticle[i])
    {
        if(P[i].Type == 0)
        {
          /// METAL & MASS distribution
            
          if(SphP[i].sniiz.MassZ > 0)
          {
            if(P[i].MassReturn - 1 < 0) // A prescription for SNII with zoom-in
            {
                
            metallicity = (P[i].Mass * P[i].Metallicity + SphP[i].sniiz.MassZ) / 
                          (P[i].Mass + P[i].MassReturn);
            if(metallicity < 1.00)
                P[i].Metallicity = metallicity;
            else 
            {
                printf("\nHold on your horses, something fishy is happening...\n");
                printf("metallicity after injection = %g\n", metallicity);
                printf("***\n Mass-MassReturn-Metallicity=%g-%g-%g\n", P[i].Mass, P[i].MassReturn, P[i].Metallicity);
                endrun(1560);
            }
           
            P[i].Mass += P[i].MassReturn; 
                
            SphP[i].sniiz.MassZ = 0.0;
            P[i].MassReturn = 0.0;
            }
            
          }
        
          if(SphP[i].sniik.Kinetic_Energy > 0)
          {
            mass         = SphP[i].StarNgbMass;
            Ngb_Den      = SphP[i].StarNgbDensity;
            Cs           = SphP[i].StarNgbSoundSpd;
            sniik_energy = SphP[i].sniik.Kinetic_Energy;
            
            snii_energy = CHABRIER_IMF2 * mass * All.UnitMass_in_g / SOLAR_MASS; // energy in units of 1e51 erg
            //      FIDUCIAL_SNII   1.0e51          // in [erg]                       //
            //      CHABRIER_IMF1   1.8e49         // in [erg M_sun^-1] from VS08    // 
            //      CHABRIER_IMF2   0.011         // in [M_sun^-1] from AGORA paper // // = total SN energy per solar mass
            t_STend = 49.3e3 * pow(snii_energy, 0.22) * pow(Ngb_Den, -0.55) * TIME_GYR; //in Gyrs
            t_fade = 1.87e6 * pow(snii_energy, 0.32) * pow(Ngb_Den, -0.37) * pow(Cs, -1.4) * TIME_GYR;
            
            SphP[i].timeST   = t_STend + time;  // Kicked ngb has to remember when to end ST phase
            SphP[i].timeFade = t_fade + time;
            SphP[i].timeExploded = time;
            
            if(P[i].Mass == 0)
            {
                printf("Beam me up Scotty ASAP.\n");
                endrun(1570);
            }
            
              
            /// SEDOV-TAYLOR velocity kick  
              
            v_shock =188 * pow(sniik_energy, 0.070) * pow(Ngb_Den, -0.140);
            if(v_shock > 1.e8 || v_shock < 0.0)
                v_shock = 0.0;
            
            v_shock /= sqrt(3.0); // Dividing by sqrt(3) for distributing the v to vx, vy and vz.  
            
            
            dir[0] = P[i].g.GravAccel[1] * P[i].Vel[2] - P[i].g.GravAccel[2] * P[i].Vel[1];
            dir[1] = P[i].g.GravAccel[2] * P[i].Vel[0] - P[i].g.GravAccel[0] * P[i].Vel[2];
            dir[2] = P[i].g.GravAccel[0] * P[i].Vel[1] - P[i].g.GravAccel[1] * P[i].Vel[0];
            
            for(k = 0, norm = 0; k < 3; k++)
                norm += dir[k] * dir[k];
            
            norm = sqrt(norm);
            if(get_random_number(P[i].ID + 5) < 0.5)
                norm = -norm;
            
            if(norm != 0)
            {
                for(k = 0; k < 3; k++)
                    dir[k] /= norm;
                
                for(k = 0; k < 3; k++)
                {
                    P[i].Vel[k] += v_shock * ascale * dir[k];
                    SphP[i].VelPred[k] += v_shock * ascale * dir[k];
                }
                
            }
            SphP[i].WindVel  = v_shock;
            SphP[i].sniik.Kinetic_Energy = 0;
            SphP[i].STkicked = 1;
           
          }
            
          if(SphP[i].STkicked == 1 && (SphP[i].timeST <= time))
             SphP[i].STkicked = 2;
            
          if(SphP[i].STkicked == 2 && SphP[i].Snowplow == 0 && 
            (SphP[i].timeFade > time))
              SphP[i].Snowplow = 1;

          if(SphP[i].Snowplow == 1 && (time >= SphP[i].timeFade))
          {
            SphP[i].STkicked = 0;
            SphP[i].Snowplow = 0;
          } 
            
        }
    } 


    return;
    
}
#endif

double snii_periodic(double x)
{
#ifdef PERIODIC   
    if(x > 0.5 * All.BoxSize)
        x -= All.BoxSize;
    
    if(x < -0.5 * All.BoxSize)
        x += All.BoxSize;
#endif
    
    return x;
}

#endif

