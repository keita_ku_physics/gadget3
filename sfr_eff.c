#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "allvars.h"
#include "proto.h"
#include "forcetree.h"


#ifdef COOLING

/*
 * This routine does cooling and star formation for
 * the effective multi-phase model.
 */

#ifdef METAL_COOLING
#include "metalcooling.h"

#ifdef ADJUSTPHYSDENSTHRESH
#define NZtable 20
static double logZtable[NZtable], PhysDensThresh_Metal[NZtable];

double PhysDensThresh_Metal_Interpol(double Z) /* Return the PhysDensThresh(Z) by table interpolation */
{
#ifdef SCHAYE_PRESSURE_MULTI
  /*  In principle, PhysDensThresh_Metal_Interpol function is disabled. */
  /*  But so far SCHAYE_PRESSURE_MULTI should not invoke ADJUSTPHYSDENSTHRESH flag*/
  return 1.0;
#else
  int nZ;
  double val, logZ;

  logZ = log10(Z);

  if((logZ >= logZtable[0]) && (logZ <= logZtable[NZtable-1]))
    {
      nZ=0;
      while ((logZtable[nZ] <= logZ) && (nZ < NZtable-1)) nZ++;
      if(nZ == 0) nZ++;
      
      val = (PhysDensThresh_Metal[nZ-1] * (logZ - logZtable[nZ]) -
             PhysDensThresh_Metal[nZ] * (logZ - logZtable[nZ-1]))
        /(logZtable[nZ-1] - logZtable[nZ]);
    }
  else
    {
      if(logZ < logZtable[0])  /* Forbid too much extrapolation toward low Z */
        val = PhysDensThresh_Metal[0];
      if(logZ > logZtable[NZtable-1]) /* Forbid too much extrapolation toward high Z */
        val = PhysDensThresh_Metal[NZtable-1];
    }
  
  return val;
#endif
}

double meanweight_neutral_fact(double Z)
{
  double meanweight, meanweight0;
  double XHydro, ne, val;

  XHydro = 1 - Y_from_Z(Z) - Z;
  /* note: assuming ne = 0.0 */
  ne = 0.0;
  meanweight = 1.0 / (XHydro + Y_from_Z(Z) / 4.0 + Z / Mz_from_Z(Z) + ne * XHydro);
  meanweight0 = 4 / (1 + 3 * HYDROGEN_MASSFRAC);

  val = meanweight/meanweight0;
  return val;
}

double meanweight_full_fact(double Z)
{
  double meanweight, meanweight0;
  double XHydro, ne, val;

  XHydro = 1 - Y_from_Z(Z) - Z;
  /* note: assuming ne = 0.0 since T=TempSupernova */
  ne = XHydro + Y_from_Z(Z)* 0.5 +Ne_from_SD(log10(All.TempSupernova),Z);
  meanweight = 1.0 / (XHydro + Y_from_Z(Z) / 4.0 + Z / Mz_from_Z(Z) + ne * XHydro);
  meanweight0 = 4 / (8 - 5 * (1 - HYDROGEN_MASSFRAC));

  val = meanweight/meanweight0;
  return val;
}
#endif
#endif

#ifdef SCHAYE_PRESSURE_MULTI
/* Kennicutt law
 * dSigma = KS_A * (Sigma/KS_SIGMA0)^KS_n (if Sigma > KS_SigmaThresh) 
 * Sigma = sqrt((GAMMA/G)*Pressure)
 */
static double KS_A        =   2.5e-4;        /* M_sun yr^-1 kpc^-2 */
static double KS_SIGMA0   =   1.0;           /* M_sun pc^-2*/
static double KS_n        =   1.4;

static double KS_SigmaThresh;                /* M_sun pc^-2*/
static double KS_Const;                      /* (KS_A^-1 * KS_SIGMA0^KS_n)*/

/*
 * P_c = K_poly * rho_c^GammaPoly for cold cloud EOS 
 * We use P_c for estimating tsfr
 */
static double K_poly;
static double fg, fth;
#endif

#if defined(VWINDS) && defined(GROUPFINDER)
#include "groupfinder.h"
void init_group_sfr(int flag)
{
  int i;
  double totbaryon;
  double init_sfr, init_sm;
  /* Set the first groupfind when SFR is init_sfr or total staris init_smtotal baryon.
   * In any case, groupfind is updated when the snapshot is dump.
   */
  init_sfr = 1e-12;  /* in M_sun yr^-1 [kpc/h]^3 */
  init_sm =  0.001;
  totbaryon =  All.OmegaBaryon * pow(All.BoxSize, 3) * (3 * All.Hubble * All.Hubble / (8 * M_PI * All.G));
  
  All.init_totsfrrate = init_sfr * pow(All.BoxSize, 3);
  All.init_total_sum_mass_stars = init_sm * totbaryon;

  if(flag == 0)
    {
      All.totsfrrate = All.prev_totsfrrate = 0.0;
      All.total_sum_mass_stars = All.prev_total_sum_mass_stars = 0.0;
      
      if(ThisTask == 0) printf("Group SFR is initailized!\n");
    }

  if(flag >= 2)
    {
      double sfrrate, sum_mass_stars;
      double totsfrrate, total_sum_mass_stars;
      sfrrate = sum_mass_stars = 0;

      for(i = 0; i < NumPart; i++)
	{
	  if(P[i].Type == 0)
	    sfrrate += SphP[i].Sfr;
	  if(P[i].Type == 4)
	    sum_mass_stars += P[i].Mass;
	}

      MPI_Allreduce(&sfrrate, &totsfrrate, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
      MPI_Allreduce(&sum_mass_stars, &total_sum_mass_stars, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

      All.prev_totsfrrate = All.totsfrrate = totsfrrate;
      All.prev_total_sum_mass_stars = All.total_sum_mass_stars = total_sum_mass_stars;

      if(ThisTask == 0) printf("In the compute_group_sfr\n");
      groupfinder_properties();
      if(ThisTask == 0) printf("Group SFR is reset!\n");
    }

  if(flag == 1)
    {
      /* It should not happen */
      if(ThisTask == 0) printf("Why do you call init_group_sfr!\n");
      endrun(9900);
    }
}

void compute_group_sfr()
{
  if(ThisTask == 0) printf("In the compute_group_sfr\n");

  groupfinder_properties();

  All.prev_totsfrrate = All.totsfrrate;
  All.prev_total_sum_mass_stars = All.total_sum_mass_stars;
}
#endif


#ifndef MHM
#ifndef SFR			/* normal cooling routine when star formation is disabled */
void cooling_and_starformation(void)
{
  int i;
  double dt, dtime, hubble_a = 0, a3inv, ne = 1;
  double time_hubble_a, unew, dmax1, dmax2;

  if(All.ComovingIntegrationOn)
    {
      /* Factors for comoving integration of hydro */
      a3inv = 1 / (All.Time * All.Time * All.Time);
      hubble_a = hubble_function(All.Time);
      time_hubble_a = All.Time * hubble_a;
    }
  else
    {
      a3inv = time_hubble_a = hubble_a = 1;
    }

  for(i = FirstActiveParticle; i >= 0; i = NextActiveParticle[i])
    {
      if(P[i].Type == 0)
	{
#ifdef WAKEUP
	  dt = P[i].dt_step * All.Timebase_interval;
#else
	  dt = (P[i].TimeBin ? (1 << P[i].TimeBin) : 0) * All.Timebase_interval;
#endif
	  /*  the actual time-step */

	  if(All.ComovingIntegrationOn)
	    dtime = All.Time * dt / time_hubble_a;
	  else
	    dtime = dt;

	  ne = SphP[i].Ne;	/* electron abundance (gives ionization state and mean molecular weight) */
        
	  unew = DoCooling(DMAX(All.MinEgySpec,
				(SphP[i].Entropy + SphP[i].e.DtEntropy * dt) /
				GAMMA_MINUS1 * pow(SphP[i].d.Density * a3inv, GAMMA_MINUS1)),
			   SphP[i].d.Density * a3inv, dtime, &ne);
        
	  SphP[i].Ne = ne;

	  if(P[i].TimeBin)	/* upon start-up, we need to protect against dt==0 */
	    {
	      if(dt > 0)
		{

#ifdef COSMIC_RAYS
		  unew += CR_Particle_ThermalizeAndDissipate(SphP + i, dtime);
#endif /* COSMIC_RAYS */

		  SphP[i].e.DtEntropy = (unew * GAMMA_MINUS1 /
					 pow(SphP[i].d.Density * a3inv, GAMMA_MINUS1) - SphP[i].Entropy) / dt;

		  if(SphP[i].e.DtEntropy < -0.5 * SphP[i].Entropy / dt)
		    SphP[i].e.DtEntropy = -0.5 * SphP[i].Entropy / dt;

		}
	    }
	}
    }
}

#else
void cooling_and_starformation(void)
/* cooling routine when star formation is enabled */
{
  int i, bin, flag, stars_spawned, tot_spawned, stars_converted, tot_converted, number_of_stars_generated;
  unsigned int bits;
  double dt, dtime, ascale = 1, hubble_a = 0, a3inv, ne = 1;
  double time_hubble_a, unew, mass_of_star;
  double sum_sm, total_sm, sm, rate, sum_mass_stars, total_sum_mass_stars;
  double p, prob;
  double cloudmass;
  double factorEVP;
  double tsfr, trelax;
  double egyhot, egyeff, egycurrent, tcool, x, y, rate_in_msunperyear;
  double sfrrate, totsfrrate, dmax1, dmax2;
  double ThreshFact, EgySpecSNFact, EgySpecColdFact;
  double a2inv, a1inv;

#ifdef BLITZ_PRESSURE
  double mu_bsfr, den_bsfr, T_bsfr, P_bsfr, egy_bsfr;
  double alpha = -0.92;
  double P0 = 4.3e4; /* critical gas pressure of the ISM when the molecular fraction is unity (Blitz & Rosolowsky '06) */
  double Gyr_in_internal_units;
#endif

#if defined(STELLAR_FEEDBACK) || defined(SNII)
  int coolingflag;
#endif
#ifdef WINDS 
  int j;
  double v, vesc;
  double norm, dir[3];
#if defined(VWINDS) && defined(GROUPFINDER)
  double VMassLoading, gal_norm_sigma;
  gal_norm_sigma = 300.0 ;
#endif

#ifdef ISOTROPICWINDS
  double theta, phi;
#endif
#endif
    
#ifdef AGORA_SF
  double t_ff;
#endif

#ifdef H2REGSF
  double Z_tmp;
  double nh0, nHeII;
  double scale_height, Chi, sval;
  double t_ff;
  double H2SigmaThresh, H2DensThresh;
  /*
  double G_Kpc_gigayear;
  double GyrInternal;
  double GInternal;
  
  G_Kpc_gigayear = 45016.932756726;
  GyrInternal    = 1./(0.98/All.HubbleParam);
  GInternal      = G_Kpc_gigayear / (GyrInternal*GyrInternal); 
	*/

  double rhotmp;
#endif

#ifdef METALS
  double w;
#endif

#ifdef COSMIC_RAYS
  double tinj, instant_reheat;
#endif

#if defined(QUICK_LYALPHA) || defined(BH_THERMALFEEDBACK) || defined (BH_KINETICFEEDBACK) || defined(SNII) || defined(STELLAR_FEEDBACK)
  double temp, u_to_temp_fac;

  u_to_temp_fac = (4 / (8 - 5 * (1 - HYDROGEN_MASSFRAC))) * PROTONMASS / BOLTZMANN * GAMMA_MINUS1
    * All.UnitEnergy_in_cgs / All.UnitMass_in_g;
#endif

#ifdef FLTROUNDOFFREDUCTION
  for(i = FirstActiveParticle; i >= 0; i = NextActiveParticle[i])
    if(P[i].Type == 0)
      SphP[i].i.Injected_BH_Energy = FLT(SphP[i].i.dInjected_BH_Energy);
#endif

  for(bin = 0; bin < TIMEBINS; bin++)
    if(TimeBinActive[bin])
      TimeBinSfr[bin] = 0;
  
  if(All.ComovingIntegrationOn)
    {
      /* Factors for comoving integration of hydro */
      a3inv = 1 / (All.Time * All.Time * All.Time);
      a2inv = 1 / (All.Time * All.Time);
      a1inv = 1 / All.Time;
      hubble_a = hubble_function(All.Time);
      time_hubble_a = All.Time * hubble_a;
      ascale = All.Time;
    }
  else
    {
      a3inv = ascale = time_hubble_a = 1;
      a2inv = 1;
      a1inv = 1;
    }
  
  stars_spawned = stars_converted = 0;
  sum_sm = sum_mass_stars = 0;
  
  for(bits = 0; GENERATIONS > (1 << bits); bits++);

  for(i = FirstActiveParticle; i >= 0; i = NextActiveParticle[i])
    {
      if(P[i].Type == 0)
	{
	  dt = (P[i].TimeBin ? (1 << P[i].TimeBin) : 0) * All.Timebase_interval;
	  /*  the actual time-step */
      //if(P[i].TimeBin) 
      //{
        //printf("P[i].TimeBin = %g (1 << P[i].TimeBin) = %g All.Timebase = %d dt=%d \n",
          //     P[i].TimeBin, (1 << P[i].TimeBin), All.Timebase_interval, dt);
        //fflush(stdout);
     // }

	  if(All.ComovingIntegrationOn)
	    dtime = All.Time * dt / time_hubble_a;
	  else
	    dtime = dt;

#ifdef METAL_COOLING
#ifdef ADJUSTPHYSDENSTHRESH  /* Test for adjust PhysDensThresh w/ metal cooling */
	  ThreshFact = PhysDensThresh_Metal_Interpol(P[i].Metallicity) / All.PhysDensThresh;
	  EgySpecSNFact = 1.0 / meanweight_full_fact(P[i].Metallicity);
	  EgySpecColdFact = 1.0 / meanweight_neutral_fact(P[i].Metallicity);
#else
	  ThreshFact = 1.0;
	  EgySpecSNFact = 1.0;
	  EgySpecColdFact = 1.0;
#endif
#else
	  ThreshFact = 1.0;
	  EgySpecSNFact = 1.0;
	  EgySpecColdFact = 1.0;
#endif
	  

#ifdef H2REGSF
	  Z_tmp = P[i].Metallicity;
	  if(Z_tmp < (All.MetalFloor * SOLAR_Z))
	    Z_tmp = All.MetalFloor * SOLAR_Z;
        
	  if(Z_tmp > 0.) //should always happen
	    {
#ifdef METAL_COOLING
	      SphP[i].XHI = 1. - Y_from_Z(Z_tmp) - Z_tmp;
#else
	      SphP[i].XHI = HYDROGEN_MASSFRAC;
#endif

	      //if a particle density goes above 0.6cm^-3 assume fully neutral (nh0=1)
	      if(SphP[i].d.Density*a3inv > All.PhysDensThresh) SphP[i].nh0 = 1.;
	      else
		{
		  ne = SphP[i].Ne;		  
#ifdef DENSITY_INDEPENDENT_SPH
		  rhotmp = SphP[i].EgyWtDensity;
#else
		  rhotmp = SphP[i].d.Density;
#endif

#ifdef METAL_COOLING
#ifdef PRIMODIAL_COOLING
		  AbundanceRatios(DMAX(All.MinEgySpec,
				       SphP[i].Entropy / GAMMA_MINUS1 * pow(rhotmp *
									    a3inv,
									    GAMMA_MINUS1)),
				  SphP[i].d.Density * a3inv, &ne, &nh0, &nHeII, 0.0);
#else
		  AbundanceRatios(DMAX(All.MinEgySpec,
				       SphP[i].Entropy / GAMMA_MINUS1 * pow(rhotmp *
									    a3inv,
									    GAMMA_MINUS1)),
				  SphP[i].d.Density * a3inv, &ne, &nh0, &nHeII, Z_tmp);
#endif			  
#else
		  
		  AbundanceRatios(DMAX(All.MinEgySpec,
				       SphP[i].Entropy / GAMMA_MINUS1 * pow(rhotmp *
									    a3inv,
									    GAMMA_MINUS1)),
				  SphP[i].d.Density * a3inv, &ne, &nh0, &nHeII);	 	  
#endif
		  SphP[i].nh0 = nh0; 
		}
	      
	      SphP[i].HIrho  = SphP[i].XHI * SphP[i].nh0 * SphP[i].d.Density;
	      SphP[i].HIgrad = sqrt(SphP[i].GradRho[0]*SphP[i].GradRho[0] + 
				    SphP[i].GradRho[1]*SphP[i].GradRho[1] + 
				    SphP[i].GradRho[2]*SphP[i].GradRho[2]);

	      if(SphP[i].HIgrad > 0. && SphP[i].HIrho > 0.)
		{
		  scale_height  = SphP[i].HIrho / SphP[i].HIgrad;
		  SphP[i].Sigma = SphP[i].HIrho * scale_height * a2inv;

		  Chi  = 3.1 * (1. + 3.1*pow(Z_tmp/SOLAR_Z,0.365)) / 4.1;
		  sval = log(1. + 0.6*Chi + 0.01*Chi*Chi)/(0.0396 * (Z_tmp/SOLAR_Z) * (SphP[i].Sigma/All.SIGMA_NORM));
		  //printf("sigma=%0.2e  sval=%0.2e\n",SphP[i].Sigma/All.SIGMA_NORM,sval);
		  if(sval < 2 && sval > 0)
		    {
		      SphP[i].fH2 = 1. - (3./4.) * (sval / (1. + 0.25*sval));
		      //calculate free fall time
		      t_ff = sqrt((3.*M_PI)/(32.*(SphP[i].d.Density*a3inv)*All.GInternal));
		    }
		  //else t_ff = SphP[i].Sigma = SphP[i].fH2 = 0.;
		  else t_ff = SphP[i].fH2 = 0.;

		  //get Sigma threshold that puts sval=2, thus allowing for the formation of H2
		  H2SigmaThresh = log(1. + 0.6*Chi + 0.01*Chi*Chi) * All.SIGMA_NORM / (0.0396 * (Z_tmp/SOLAR_Z) * 2.);
		  //convert surface density to volume density
		  H2DensThresh  = H2SigmaThresh/(scale_height * a1inv);
		  //rescale H2 volume density to total gas volume density
		  H2DensThresh /= (SphP[i].XHI*SphP[i].nh0);
		}
	      else
		{
		  printf("HIGRAD = %g!! XHI=%g, nh0=%g, GradRho0=%g, GradRho1=%g, GradRho2=%g,   Z=%g for PID=%d \n",
			 SphP[i].HIgrad,SphP[i].XHI,SphP[i].nh0,SphP[i].GradRho[0],SphP[i].GradRho[1],SphP[i].GradRho[2],Z_tmp,P[i].ID);
		  scale_height = SphP[i].Sigma = Chi = sval = SphP[i].fH2 = t_ff = 0.;
		  //if HIgrad or HIrho are <= 0 then set density thresholds very high
		  H2SigmaThresh = H2DensThresh = (All.PhysDensThresh * 1.0e20); 
		}
	    }
	  else
	    {
	      t_ff = scale_height = sval = Chi = 0.;
	      SphP[i].Sigma = SphP[i].fH2 = 0.;
	      H2DensThresh  = (All.PhysDensThresh * 1.0e20);
	    }
#endif //H2REGSF

#ifdef AGORA_SF
    t_ff = sqrt((3.*M_PI)/(32.*(SphP[i].d.Density*a3inv)*All.G));
#endif

	  /* check whether conditions for star formation are fulfilled.
	   *  
	   * f=1  normal cooling
	   * f=0  star formation
	   */
	  flag = 1;		/* default is normal cooling */


#ifdef H2REGSF
	  if(SphP[i].fH2 > 0.)
	    flag = 0;
	  
	  if(All.ComovingIntegrationOn)
	    if(SphP[i].d.Density < All.OverDensThresh)
	      flag = 1;

	  //temporary SCHAYE model parameters to allow for the calculation of egyhot
	  //double P_c;   /* Cold cloud pressure */
	  //double ks_sigma;
	  //P_c = K_poly * pow(SphP[i].d.Density * a3inv,All.GammaPoly);
	  //ks_sigma = sqrt((GAMMA / All.G) * P_c * fg / fth);
#else //h2reg
#ifdef SCHAYE_PRESSURE_MULTI
	  double P_c;   /* Cold cloud pressure */
	  double ks_sigma;
	  P_c = K_poly * pow(SphP[i].d.Density * a3inv,All.GammaPoly);
	  ks_sigma = sqrt((GAMMA / All.G) * P_c * fg / fth);
	  
	  if(ks_sigma >= KS_SigmaThresh)             /* ORIGINAL */
	    flag = 0;
	  
	  //JASON'S TEST
	  //if(ks_sigma >= KS_SigmaThresh/All.Time)  /* JASONS MODEL */
	  //  flag = 0;
	  
	  if(All.ComovingIntegrationOn)
	    if(SphP[i].d.Density < All.OverDensThresh)
	      flag = 1;
#else //schaye_pressure_multi
	  if(SphP[i].d.Density * a3inv >= All.PhysDensThresh * ThreshFact)
	    flag = 0;

	  if(All.ComovingIntegrationOn)
	    if(SphP[i].d.Density < All.OverDensThresh)
	      flag = 1;
#endif //shaye_pressure_multi
#endif //h2regsf

#ifdef BLACK_HOLES
	  if(P[i].Mass == 0)
	    flag = 1;
#endif
        
#ifdef SNII
      if(SphP[i].STkicked == 1)
        flag = 1;      /* adiabatic expansion without radiative cooling */

#endif
#ifdef WINDS
	  if(SphP[i].DelayTime > 0)
	    flag = 1;		/* only normal cooling for particles in the wind */

	  if(SphP[i].DelayTime > 0)
	    SphP[i].DelayTime -= dtime;

#ifdef H2REGSF
	  if(SphP[i].DelayTime > 0)
	    if(SphP[i].d.Density * a3inv < All.WindFreeTravelDensFac * H2DensThresh * ThreshFact)
	      SphP[i].DelayTime = 0;
#else
	  if(SphP[i].DelayTime > 0)
	    if(SphP[i].d.Density * a3inv < All.WindFreeTravelDensFac * All.PhysDensThresh * ThreshFact)
	      SphP[i].DelayTime = 0;
#endif //H2REGSF

	  if(SphP[i].DelayTime < 0)
	    SphP[i].DelayTime = 0;
#endif


#ifdef QUICK_LYALPHA
	  temp = u_to_temp_fac * (SphP[i].Entropy + SphP[i].e.DtEntropy * dt) /
	    GAMMA_MINUS1 * pow(SphP[i].d.Density * a3inv, GAMMA_MINUS1);

	  if(SphP[i].d.Density > All.OverDensThresh && temp < 1.0e5)
	    flag = 0;
	  else
	    flag = 1;
#endif


#if !defined(NOISMPRESSURE) && !defined(QUICK_LYALPHA)
	  if(flag == 1)		/* normal implicit isochoric cooling */
#endif
	    {
	      SphP[i].Sfr = 0;
#if defined(COSMIC_RAYS) && defined(CR_OUTPUT_INJECTION)
	      SphP[i].CR_Specific_SupernovaHeatingRate = 0;
#endif
	      ne = SphP[i].Ne;	/* electron abundance (gives ionization state and mean molecular weight) */


	      unew = DMAX(All.MinEgySpec,
			  (SphP[i].Entropy + SphP[i].e.DtEntropy * dt) /
			  GAMMA_MINUS1 * pow(SphP[i].d.Density * a3inv, GAMMA_MINUS1));

#if defined(BH_THERMALFEEDBACK) || defined(BH_KINETICFEEDBACK)
	      if(SphP[i].i.Injected_BH_Energy)
		{
		  if(P[i].Mass == 0)
		    SphP[i].i.Injected_BH_Energy = 0;
		  else
		    unew += SphP[i].i.Injected_BH_Energy / P[i].Mass;

		  temp = u_to_temp_fac * unew;


		  if(temp > 5.0e9)
		    unew = 5.0e9 / u_to_temp_fac;

#ifdef FLTROUNDOFFREDUCTION
		  SphP[i].i.dInjected_BH_Energy = 0;
#else
		  SphP[i].i.Injected_BH_Energy = 0;
#endif
		}
#endif


#ifdef STELLAR_FEEDBACK
        if(SphP[i].sniis.Energy)
      {
        if(P[i].Mass == 0)
            SphP[i].sniis.Energy = 0;
        else
            unew += SphP[i].sniis.Energy / P[i].Mass;
                
        temp = u_to_temp_fac * unew;
        //printf("*** *** *** stellar feedback: temp = %g\n", temp);
        //SphP[i].SFBtemp = temp;
        if(temp > 5.0e9)
            unew = 5.0e9 / u_to_temp_fac;
            
        SphP[i].sniis.Energy = 0;  
           
      }    
#endif
#ifdef SNII            
          if(SphP[i].sniith.Thermal_Energy)
        {
          if(P[i].Mass == 0)
            SphP[i].sniith.Thermal_Energy = 0;
          else
            unew += SphP[i].sniith.Thermal_Energy / P[i].Mass;
            
          temp = u_to_temp_fac * unew;
          
          if(temp > 5.0e9)
            unew = 5.0e9 / u_to_temp_fac;
            
          SphP[i].sniith.Thermal_Energy = 0;  
        }
#endif

#ifdef SNII
            if(SphP[i].STkicked != 1)
#endif
            {
#ifdef METAL_COOLING
#ifdef PRIMODIAL_COOLING  /* In case a simulation uses only SD93 primodial cooling. 
			   * Recommand use with disable ADJUSTPHYSDENSTHRESH flag.
			   */ 
	      unew = DoCooling(unew, SphP[i].d.Density * a3inv, dtime, &ne, 0.0);
#else
	      unew = DoCooling(unew, SphP[i].d.Density * a3inv, dtime, &ne, P[i].Metallicity);
#endif
#else
	      unew = DoCooling(unew, SphP[i].d.Density * a3inv, dtime, &ne);
#endif
            }

	      ///SphP[i].Ne = ne; // Apr 15 Tues
        
	      if(P[i].TimeBin)	/* upon start-up, we need to protect against dt==0 */
		{
		  /* note: the adiabatic rate has been already added in ! */

		  if(dt > 0)
		    {
#ifdef COSMIC_RAYS
		      unew += CR_Particle_ThermalizeAndDissipate(SphP + i, dtime);
#endif /* COSMIC_RAYS */


		      SphP[i].e.DtEntropy = (unew * GAMMA_MINUS1 /
					     pow(SphP[i].d.Density * a3inv,
						 GAMMA_MINUS1) - SphP[i].Entropy) / dt;

		      if(SphP[i].e.DtEntropy < -0.5 * SphP[i].Entropy / dt)
			SphP[i].e.DtEntropy = -0.5 * SphP[i].Entropy / dt;
		    }
		}            
            
	      /* Winds for low denisty rho < rho_th gas in the galaxy */
#ifdef WINDS
#if defined(VWINDS) && defined(GROUPFINDER)
	      /* Only the variable wind model is Implemented for cold wind*/
	      if(P[i].Type == 0 && P[i].GroupMassGas > 0.0 && P[i].GroupSfr > 0.0) 
		{
		  /* Now wind velocity is determined first based on escape velocity */
		  vesc = 150.0 * pow(P[i].GroupSfr, 1./3.) / sqrt(4.0 * All.Time);
		  v = vesc;

		  /* Mass loading factor based on galaxy size (sigma ~ 0.5*v_esc) */
		  /* Low density region (default as Energy Driven)
		   * Momentum Dirven :
		   * VMassLoading = gal_norm_sigma / (0.5 * vesc)
		   * Energy Driven :
		   * VMassLoading = (gal_norm_sigma * gal_norm_sigma) / (0.25 * vesc * vesc)
		   */
		  VMassLoading = (gal_norm_sigma * gal_norm_sigma) / (0.25 * vesc * vesc);

		  /* Note the Sfr is in unit of Msolar/yr */
		  p = VMassLoading * dtime / P[i].GroupMassGas
		    * P[i].GroupSfr / (All.UnitMass_in_g / SOLAR_MASS) * (All.UnitTime_in_s / SEC_PER_YEAR);
		  prob = 1 - exp(-p);

		  if(get_random_number(P[i].ID + 2) < prob)	/* ok, make the particle go into the wind */
		    {

#ifdef ISOTROPICWINDS
		      theta = acos(2 * get_random_number(P[i].ID + 3) - 1);
		      phi = 2 * M_PI * get_random_number(P[i].ID + 4);

		      dir[0] = sin(theta) * cos(phi);
		      dir[1] = sin(theta) * sin(phi);
		      dir[2] = cos(theta);
#else
		      dir[0] = P[i].g.GravAccel[1] * P[i].Vel[2] - P[i].g.GravAccel[2] * P[i].Vel[1];
		      dir[1] = P[i].g.GravAccel[2] * P[i].Vel[0] - P[i].g.GravAccel[0] * P[i].Vel[2];
		      dir[2] = P[i].g.GravAccel[0] * P[i].Vel[1] - P[i].g.GravAccel[1] * P[i].Vel[0];
#endif

		      for(j = 0, norm = 0; j < 3; j++)
			norm += dir[j] * dir[j];

		      norm = sqrt(norm);
		      if(get_random_number(P[i].ID + 5) < 0.5)
			norm = -norm;

		      if(norm != 0)
			{
			  for(j = 0; j < 3; j++)
			    dir[j] /= norm;

			  for(j = 0; j < 3; j++)
			    {
			      P[i].Vel[j] += v * ascale * dir[j];
			      SphP[i].VelPred[j] += v * ascale * dir[j];
			    }
			  /* Wind Travel length is the size of galaxy
			   * which is the maximum extend of the galaxy from its densiest particle
			   */
			  if(v > 0) SphP[i].DelayTime = P[i].GroupSize / v;
			}
		    }
		}
#endif
#endif
	    }

	  if(flag == 0)		/* active star formation */
	    {
#if !defined(QUICK_LYALPHA)	      
#ifdef H2REGSF
	      tsfr = t_ff / H2REG_EFF;
	      factorEVP = pow(SphP[i].d.Density * a3inv / (H2DensThresh * ThreshFact), -0.8) * All.FactorEVP;
#else //h2reg
#ifdef AGORA_SF
          tsfr = t_ff / AGORA_SF;
#else
#ifdef SCHAYE_PRESSURE_MULTI
	      tsfr = KS_Const * pow(ks_sigma, (1.0 - KS_n));
#else /* schaye_pressure_multi */
#ifdef BLITZ_PRESSURE
	      /* From KN 2006 But include metallicity effect */
	      /* assuming the neutral gas bc density is high */
	      mu_bsfr = 1.0 / (1 - 0.75 * Y_from_Z(P[i].Metallicity) - (1.0 - 1.0 / Mz_from_Z(P[i].Metallicity)) * P[i].Metallicity);
	      egy_bsfr = SphP[i].Entropy * pow(SphP[i].d.Density * a3inv, GAMMA_MINUS1) / GAMMA_MINUS1;
	      T_bsfr = mu_bsfr / BOLTZMANN * GAMMA_MINUS1 * egy_bsfr * All.UnitEnergy_in_cgs/ All.UnitMass_in_g;
	      if(T_bsfr < 0.1) T_bsfr = 0.1;

	      /* Compute pressure, i.e. P/k [K cm^-3] */
	      /* Since SphP.mu includes proton mass, this is the number density of atoms in cm^-3 */
	      den_bsfr = SphP[i].d.Density * All.UnitMass_in_g * pow(All.HubbleParam, 2) / pow(All.UnitLength_in_cm, 3) / mu_bsfr * a3inv;
	      P_bsfr = den_bsfr * T_bsfr; /* this is equal to P/k [K cm^-3] */

	      /* Blitz sfr was per Gyr. Gadget internal time unit is 9.8e8 yr/h = 1.4 Gyr for h=0.7.
		 So 1 Gyr = (1./(0.98/h)) in internal units. */
	      Gyr_in_internal_units = 1. / (0.98 / All.HubbleParam);

	      /* Bliz_factor = 1/tsfr */
	      if(P_bsfr < P0)
		tsfr = (1.+ pow(P_bsfr / P0, alpha)) * Gyr_in_internal_units;
	      else
		tsfr = sqrt((All.PhysDensThresh * ThreshFact) / (SphP[i].d.Density * a3inv)) * All.MaxSfrTimescale;
#else  /* blitz_pressue	*/      
	      tsfr = sqrt((All.PhysDensThresh * ThreshFact) / (SphP[i].d.Density * a3inv)) * All.MaxSfrTimescale;
#endif /* BLITZ_PRESSURE */
#endif /* SCHAYE_PRESSURE_MULTI */
#endif
	      factorEVP = pow(SphP[i].d.Density * a3inv / (All.PhysDensThresh * ThreshFact), -0.8) * All.FactorEVP;
#endif /* H2REGSF */

	      egyhot = (All.EgySpecSN * EgySpecSNFact) / (1 + factorEVP) + (All.EgySpecCold * EgySpecColdFact);

	      ne = SphP[i].Ne;	

#ifdef METAL_COOLING
#ifdef PRIMODIAL_COOLING
	      tcool = GetCoolingTime(egyhot, SphP[i].d.Density * a3inv, &ne, 0.0);
#else /* primordial_cooling */
	      tcool = GetCoolingTime(egyhot, SphP[i].d.Density * a3inv, &ne, P[i].Metallicity);
#endif /* PRIMORDIAL_COOLING */
#else /* metal_cooling */
	      tcool = GetCoolingTime(egyhot, SphP[i].d.Density * a3inv, &ne);
#endif /* METAL_COOLING */
	      SphP[i].Ne = ne;
#ifdef DT_COOL
	      if (tcool > 0.0) SphP[i].t_cool = tcool;
#endif /*DT_COOL*/

	      y =
		tsfr / tcool * egyhot / (All.FactorSN * (All.EgySpecSN * EgySpecSNFact) 
					 - (1 - All.FactorSN) * (All.EgySpecCold * EgySpecColdFact));

	      x = 1 + 1 / (2 * y) - sqrt(1 / y + 1 / (4 * y * y));

	      egyeff = egyhot * (1 - x) + (All.EgySpecCold * EgySpecColdFact)* x;

#ifdef H2REGSF
	      cloudmass = SphP[i].fH2 * SphP[i].XHI * SphP[i].nh0 * P[i].Mass;
#else
	      cloudmass = x * P[i].Mass;
#endif

	      if(tsfr < dtime)
		tsfr = dtime;
#if defined(STELLAR_FEEDBACK) || defined(SNII)
          sm = dtime / tsfr * cloudmass;  
#else
	      sm = (1 - All.FactorSN) * dtime / tsfr * cloudmass;	/* amount of stars expect to form */
#endif
	      p = sm / P[i].Mass;
	      
	      sum_sm += P[i].Mass * (1 - exp(-p));


	      if(dt > 0)
		{
		  if(P[i].TimeBin)	/* upon start-up, we need to protect against dt==0 */
		    {
		      trelax = tsfr * (1 - x) / x / (All.FactorSN * (1 + factorEVP));
		      egycurrent =
			SphP[i].Entropy * pow(SphP[i].d.Density * a3inv, GAMMA_MINUS1) / GAMMA_MINUS1;

#ifdef COSMIC_RAYS
		      if(All.CR_SNEff > 0)
			{
			  tinj = SphP[i].CR_E0 / (p * All.FeedbackEnergy * All.CR_SNEff / dtime);

			  instant_reheat =
			    CR_Particle_SupernovaFeedback(&SphP[i], p * All.FeedbackEnergy * All.CR_SNEff,
							  tinj);
			}
		      else
			instant_reheat = 0;

#if defined(COSMIC_RAYS) && defined(CR_OUTPUT_INJECTION)
		      SphP[i].CR_Specific_SupernovaHeatingRate =
			(p * All.FeedbackEnergy * All.CR_SNEff - instant_reheat) / dtime;
#endif

		      egycurrent += instant_reheat;

		      egycurrent += CR_Particle_ThermalizeAndDissipate(SphP + i, dtime);
#endif /* COSMIC_RAYS */


#if defined(BH_THERMALFEEDBACK) || defined(BH_KINETICFEEDBACK)
		      if(SphP[i].i.Injected_BH_Energy > 0)
			{
			  egycurrent += SphP[i].i.Injected_BH_Energy / P[i].Mass;

			  temp = u_to_temp_fac * egycurrent;

			  if(temp > 5.0e9)
			    egycurrent = 5.0e9 / u_to_temp_fac;

			  if(egycurrent > egyeff)
			    {
#ifdef METAL_COOLING
#ifdef PRIMODIAL_COOLING
			      tcool = GetCoolingTime(egycurrent, SphP[i].d.Density * a3inv, &ne, 0.0);
#else
			      tcool = GetCoolingTime(egycurrent, SphP[i].d.Density * a3inv, &ne, P[i].Metallicity);
#endif
#else
			      tcool = GetCoolingTime(egycurrent, SphP[i].d.Density * a3inv, &ne);
#endif
			      if(tcool < trelax && tcool > 0)
				trelax = tcool;
			    }

			  SphP[i].i.Injected_BH_Energy = 0;
			}
#endif /*if!defined (QUICK_LYALPHA) */

                
#if !defined(NOISMPRESSURE)
		      SphP[i].Entropy =
			(egyeff +
			 (egycurrent -
			  egyeff) * exp(-dtime / trelax)) * GAMMA_MINUS1 /
			pow(SphP[i].d.Density * a3inv, GAMMA_MINUS1);

		      SphP[i].e.DtEntropy = 0;
#endif
		    }
		}



	      /* the upper bits of the gas particle ID store how man stars this gas
	         particle gas already generated */

	      if(bits == 0)
		number_of_stars_generated = 0;
	      else
		number_of_stars_generated = (P[i].ID >> (32 - bits));

	      mass_of_star = P[i].Mass / (GENERATIONS - number_of_stars_generated);
            
#if defined(STELLAR_FEEDBACK) || defined(SNII)
          SphP[i].Sfr = cloudmass / tsfr *
                        (All.UnitMass_in_g / SOLAR_MASS) / (All.UnitTime_in_s / SEC_PER_YEAR);
#else
	      SphP[i].Sfr = (1 - All.FactorSN) * cloudmass / tsfr *
		(All.UnitMass_in_g / SOLAR_MASS) / (All.UnitTime_in_s / SEC_PER_YEAR);
#endif
	      TimeBinSfr[P[i].TimeBin] += SphP[i].Sfr;
#ifdef METALS
//#ifndef STELLAR_FEEDBACK
//#ifndef SNII
	      w = get_random_number(P[i].ID);
	      P[i].Metallicity += w * METAL_YIELD * (1 - exp(-p));
//#endif
//#endif
#endif

	      prob = P[i].Mass / mass_of_star * (1 - exp(-p));

#else /* belongs to ifndef(QUICK_LYALPHA) */

	      prob = 2.0;	/* this will always cause a star creation event */

	      if(bits == 0)
		number_of_stars_generated = 0;
	      else
		number_of_stars_generated = (P[i].ID >> (32 - bits));

	      mass_of_star = P[i].Mass / (GENERATIONS - number_of_stars_generated);

	      SphP[i].Sfr = 0;

#endif /* ends to QUICK_LYALPHA */

	      if(get_random_number(P[i].ID + 1) < prob)	/* ok, make a star */
		{
		  if(number_of_stars_generated == (GENERATIONS - 1))
		    {
		      /* here we turn the gas particle itself into a star */
		      Stars_converted++;
		      stars_converted++;

		      sum_mass_stars += P[i].Mass;

		      P[i].Type = 4;
		      TimeBinCountSph[P[i].TimeBin]--;
		      TimeBinSfr[P[i].TimeBin] -= SphP[i].Sfr;

#ifdef STELLARAGE
		      P[i].StellarAge = All.Time;
#endif

#ifdef STELLAR_FEEDBACK
              P[i].StellarFB_Energy = All.StellarFBEfficiency * STELLAR_FB_ENERGY / All.UnitEnergy_in_cgs * mass_of_star * All.UnitMass_in_g / SOLAR_MASS;
              P[i].DepositedTime = All.Time; // initializing same as the stellar age
#endif
#if defined(STELLAR_FEEDBACK) || defined(SNII)
              P[i].Exploded = 0;
#endif
		    }
		  else
		    {
		      /* here we spawn a new star particle */

		      if(NumPart + stars_spawned >= All.MaxPart)
			{
			  printf
			    ("On Task=%d with NumPart=%d we try to spawn %d particles. Sorry, no space left...(All.MaxPart=%d)\n",
			     ThisTask, NumPart, stars_spawned, All.MaxPart);
			  fflush(stdout);
			  endrun(8888);
			}

		      P[NumPart + stars_spawned] = P[i];
		      P[NumPart + stars_spawned].Type = 4;

		      NextActiveParticle[NumPart + stars_spawned] = FirstActiveParticle;
		      FirstActiveParticle = NumPart + stars_spawned;
		      NumForceUpdate++;

		      TimeBinCount[P[NumPart + stars_spawned].TimeBin]++;

		      PrevInTimeBin[NumPart + stars_spawned] = i;
		      NextInTimeBin[NumPart + stars_spawned] = NextInTimeBin[i];
		      if(NextInTimeBin[i] >= 0)
			PrevInTimeBin[NextInTimeBin[i]] = NumPart + stars_spawned;
		      NextInTimeBin[i] = NumPart + stars_spawned;
		      if(LastInTimeBin[P[i].TimeBin] == i)
			LastInTimeBin[P[i].TimeBin] = NumPart + stars_spawned;

		      P[i].ID += (1 << (32 - bits));

		      P[NumPart + stars_spawned].Mass = mass_of_star;
		      P[i].Mass -= P[NumPart + stars_spawned].Mass;
		      sum_mass_stars += P[NumPart + stars_spawned].Mass;
#ifdef STELLARAGE
		      P[NumPart + stars_spawned].StellarAge = All.Time;
#endif

#ifdef STELLAR_FEEDBACK
              P[NumPart + stars_spawned].StellarFB_Energy = All.StellarFBEfficiency * STELLAR_FB_ENERGY / All.UnitEnergy_in_cgs * mass_of_star * All.UnitMass_in_g / SOLAR_MASS;
              P[NumPart + stars_spawned].DepositedTime = All.Time;
#endif
#if defined(STELLAR_FEEDBACK) || defined(SNII)
              P[NumPart + stars_spawned].Exploded = 0;
#endif
		      force_add_star_to_tree(i, NumPart + stars_spawned);

		      stars_spawned++;
		    }
		}

#ifdef METALS
//#ifndef STELLAR_FEEDBACK
//#ifndef SNII
	      if(P[i].Type == 0)	/* to protect using a particle that has been turned into a star */
		P[i].Metallicity += (1 - w) * METAL_YIELD * (1 - exp(-p));
//#endif
//#endif
#endif

#ifdef WINDS
#if defined(VWINDS) && defined(GROUPFINDER)
	      /* Here comes the variable wind model */
	      if(P[i].Type == 0 && P[i].GroupMassGas > 0.0 && P[i].GroupSfr > 0.0) 
		{
                  /* Now wind velocity is determined first based on escape velocity */
                  vesc = 150.0 * pow(P[i].GroupSfr, 1./3.) / sqrt(4.0 * All.Time);
		  /* accelerated by continuous radiation pressure : default 1.5 times */
                  v = 1.5 * vesc;

		  /* Mass loading factor based on galaxy size (sigma ~ 0.5*v_esc) */
		  /* High density region (default as Momentum Driven)
		   * Momentum Dirven :
		   * VMassLoading = gal_norm_sigma / (0.5 * vesc)
		   * Energy Driven :
		   * VMassLoading = (gal_norm_sigma * gal_norm_sigma) / (0.25 * vesc * vesc)
		   */
                  VMassLoading = gal_norm_sigma / (0.5 * vesc);

		  /* Note the Sfr is in unit of Msolar/yr */
		  p = VMassLoading * dtime / P[i].GroupMassGas
		    * P[i].GroupSfr / (All.UnitMass_in_g / SOLAR_MASS) * (All.UnitTime_in_s / SEC_PER_YEAR);
		  prob = 1 - exp(-p);

		  if(get_random_number(P[i].ID + 2) < prob)	/* ok, make the particle go into the wind */
		    {

#ifdef ISOTROPICWINDS
		      theta = acos(2 * get_random_number(P[i].ID + 3) - 1);
		      phi = 2 * M_PI * get_random_number(P[i].ID + 4);

		      dir[0] = sin(theta) * cos(phi);
		      dir[1] = sin(theta) * sin(phi);
		      dir[2] = cos(theta);
#else
		      dir[0] = P[i].g.GravAccel[1] * P[i].Vel[2] - P[i].g.GravAccel[2] * P[i].Vel[1];
		      dir[1] = P[i].g.GravAccel[2] * P[i].Vel[0] - P[i].g.GravAccel[0] * P[i].Vel[2];
		      dir[2] = P[i].g.GravAccel[0] * P[i].Vel[1] - P[i].g.GravAccel[1] * P[i].Vel[0];
#endif

		      for(j = 0, norm = 0; j < 3; j++)
			norm += dir[j] * dir[j];

		      norm = sqrt(norm);
		      if(get_random_number(P[i].ID + 5) < 0.5)
			norm = -norm;

		      if(norm != 0)
			{
			  for(j = 0; j < 3; j++)
			    dir[j] /= norm;

			  for(j = 0; j < 3; j++)
			    {
			      P[i].Vel[j] += v * ascale * dir[j];
			      SphP[i].VelPred[j] += v * ascale * dir[j];
			    }
			  /* Wind Travel length is the size of galaxy
			   * which is the maximum extend of the galaxy from its densiest particle
			   */
			  if(v > 0) SphP[i].DelayTime = P[i].GroupSize / v;
			}
		    }
		}
#else
	      /* Here comes the wind model */
	      if(P[i].Type == 0)	/* to protect using a particle that has been turned into a star */
		{
		  p = All.WindEfficiency * sm / P[i].Mass;

		  prob = 1 - exp(-p);

		  if(get_random_number(P[i].ID + 2) < prob)	/* ok, make the particle go into the wind */
		    {
		      v =
			sqrt(2 * All.WindEnergyFraction * All.FactorSN * (All.EgySpecSN * EgySpecSNFact) 
			     / (1 - All.FactorSN) / All.WindEfficiency);
#ifdef ISOTROPICWINDS
		      theta = acos(2 * get_random_number(P[i].ID + 3) - 1);
		      phi = 2 * M_PI * get_random_number(P[i].ID + 4);

		      dir[0] = sin(theta) * cos(phi);
		      dir[1] = sin(theta) * sin(phi);
		      dir[2] = cos(theta);
#else
		      dir[0] = P[i].g.GravAccel[1] * P[i].Vel[2] - P[i].g.GravAccel[2] * P[i].Vel[1];
		      dir[1] = P[i].g.GravAccel[2] * P[i].Vel[0] - P[i].g.GravAccel[0] * P[i].Vel[2];
		      dir[2] = P[i].g.GravAccel[0] * P[i].Vel[1] - P[i].g.GravAccel[1] * P[i].Vel[0];
#endif

		      for(j = 0, norm = 0; j < 3; j++)
			norm += dir[j] * dir[j];

		      norm = sqrt(norm);
		      if(get_random_number(P[i].ID + 5) < 0.5)
			norm = -norm;

		      if(norm != 0)
			{
			  for(j = 0; j < 3; j++)
			    dir[j] /= norm;

			  for(j = 0; j < 3; j++)
			    {
			      P[i].Vel[j] += v * ascale * dir[j];
			      SphP[i].VelPred[j] += v * ascale * dir[j];
			    }

			  SphP[i].DelayTime = All.WindFreeTravelLength / v;
			}
		    }
		}
#endif
#endif
	    }
	}

    }				/* end of main loop over active particles */


  MPI_Allreduce(&stars_spawned, &tot_spawned, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
  MPI_Allreduce(&stars_converted, &tot_converted, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
  if(tot_spawned > 0 || tot_converted > 0)
    {
      if(ThisTask == 0)
	{
	  printf("SFR: spawned %d stars, converted %d gas particles into stars\n",
		 tot_spawned, tot_converted);
	  fflush(stdout);
	}


      All.TotNumPart += tot_spawned;
      All.TotN_gas -= tot_converted;
      NumPart += stars_spawned;

      /* Note: N_gas is only reduced once rearrange_particle_sequence is called */

      /* Note: New tree construction can be avoided because of  `force_add_star_to_tree()' */
    }

  for(bin = 0, sfrrate = 0; bin < TIMEBINS; bin++)
    if(TimeBinCount[bin])
      sfrrate += TimeBinSfr[bin];

  MPI_Allreduce(&sfrrate, &totsfrrate, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

  MPI_Reduce(&sum_sm, &total_sm, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Allreduce(&sum_mass_stars, &total_sum_mass_stars, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

#if defined(VWINDS) && defined(GROUPFINDER)
  All.totsfrrate = totsfrrate;
  All.total_sum_mass_stars += total_sum_mass_stars;
#endif

  if(ThisTask == 0)
    {
      if(All.TimeStep > 0)
	rate = total_sm / (All.TimeStep / time_hubble_a);
      else
	rate = 0;

      /* convert to solar masses per yr */

      rate_in_msunperyear = rate * (All.UnitMass_in_g / SOLAR_MASS) / (All.UnitTime_in_s / SEC_PER_YEAR);

      fprintf(FdSfr, "%g %g %g %g %g\n", All.Time, total_sm, totsfrrate, rate_in_msunperyear,
	      total_sum_mass_stars);
      fflush(FdSfr);
    }
}


double get_starformation_rate(int i)
{
  double rateOfSF;
  double a3inv;
  int flag;

  double ThreshFact, EgySpecSNFact, EgySpecColdFact;
  double tsfr, cloudmass;

#ifdef AGORA_SF
  double t_ff;
#endif
    
#ifdef H2REGSF
  double t_ff;
  /*
  double GInternal, GyrInternal, G_Kpc_gigayear, stogyr, G_internalunit;
  stogyr = 3.15569e16;
  G_internalunit = All.UnitLength_in_cm/(All.UnitLength_in_cm*All.UnitLength_in_cm*All.UnitLength_in_cm)*(stogyr*stogyr);
  //G_Kpc_gigayear = 45016.932756726;
  GyrInternal    = 1./(0.98/All.HubbleParam);
  GInternal      = G_internalunit / (GyrInternal*GyrInternal);
  */
#else
  double factorEVP, egyhot, ne, tcool, y, x;
#endif


#ifdef BLITZ_PRESSURE
  double mu_bsfr, den_bsfr, T_bsfr, P_bsfr, egy_bsfr;
  double alpha = -0.92;
  double P0 = 4.3e4; /* critical gas pressure of the ISM when the molecular fraction is unity (Blitz & Rosolowsky '06) */
  double Gyr_in_internal_units;
#endif

#ifdef METAL_COOLING
#ifdef ADJUSTPHYSDENSTHRESH  /* Test for adjust PhysDensThresh w/ metal cooling */
	  ThreshFact = PhysDensThresh_Metal_Interpol(P[i].Metallicity) / All.PhysDensThresh;
	  EgySpecSNFact = 1.0 / meanweight_full_fact(P[i].Metallicity);
	  EgySpecColdFact = 1.0 / meanweight_neutral_fact(P[i].Metallicity);
#else
	  ThreshFact = 1.0;
	  EgySpecSNFact = 1.0;
	  EgySpecColdFact = 1.0;
#endif
#else
	  ThreshFact = 1.0;
	  EgySpecSNFact = 1.0;
	  EgySpecColdFact = 1.0;
#endif


  if(All.ComovingIntegrationOn)
    a3inv = 1 / (All.Time * All.Time * All.Time);
  else
    a3inv = 1;


  flag = 1;			/* default is normal cooling */


#ifdef H2REGSF
  if(SphP[i].fH2 > 0.)
    flag = 0;

  if(All.ComovingIntegrationOn)
    if(SphP[i].d.Density < All.OverDensThresh)
      flag = 1;
#else
#ifdef SCHAYE_PRESSURE_MULTI
  double P_c;   /* Cold cloud pressure */
  double ks_sigma;
  P_c = K_poly * pow(SphP[i].d.Density * a3inv, All.GammaPoly);
  ks_sigma = sqrt((GAMMA / All.G) * P_c * fg / fth);
  
  if(ks_sigma >= KS_SigmaThresh)
    flag = 0;
  
  if(All.ComovingIntegrationOn)
    if(SphP[i].d.Density < All.OverDensThresh)
      flag = 1;
#else
  if(SphP[i].d.Density * a3inv >= All.PhysDensThresh * ThreshFact)
    flag = 0;
  
  if(All.ComovingIntegrationOn)
    if(SphP[i].d.Density < All.OverDensThresh)
      flag = 1;
#endif
#endif

  if(flag == 1)
    return 0;

#ifdef H2REGSF
  t_ff      = sqrt((3.*M_PI)/(32.*(SphP[i].d.Density*a3inv)*All.GInternal));
  tsfr      = t_ff/H2REG_EFF;
  cloudmass = SphP[i].fH2 * SphP[i].XHI * SphP[i].nh0 * P[i].Mass;
#else
#ifdef AGORA_SF
  t_ff = sqrt((3.*M_PI)/(32.*(SphP[i].d.Density*a3inv)*All.G));
  tsfr = t_ff/AGORA_SF;
#else
#ifdef SCHAYE_PRESSURE_MULTI
  tsfr = KS_Const * pow(ks_sigma, (1.0 - KS_n));
#else
#ifdef BLITZ_PRESSURE
  mu_bsfr = 1.0 / (1 - 0.75 * Y_from_Z(P[i].Metallicity) - (1.0 - 1.0 / Mz_from_Z(P[i].Metallicity)) * P[i].Metallicity);
  egy_bsfr = SphP[i].Entropy * pow(SphP[i].d.Density * a3inv, GAMMA_MINUS1) / GAMMA_MINUS1;
  T_bsfr = mu_bsfr / BOLTZMANN * GAMMA_MINUS1 * egy_bsfr * All.UnitEnergy_in_cgs/ All.UnitMass_in_g;
  if(T_bsfr < 0.1) T_bsfr = 0.1;

  den_bsfr = SphP[i].d.Density * All.UnitMass_in_g * pow(All.HubbleParam, 2) / pow(All.UnitLength_in_cm, 3) / mu_bsfr * a3inv;
  P_bsfr = den_bsfr * T_bsfr; /* this is equal to P/k [K cm^-3] */

  Gyr_in_internal_units = 1. / (0.98 / All.HubbleParam);

  if(P_bsfr < P0)
    tsfr = (1.+ pow(P_bsfr / P0, alpha)) * Gyr_in_internal_units;
  else
    tsfr = sqrt((All.PhysDensThresh * ThreshFact) / (SphP[i].d.Density * a3inv)) * All.MaxSfrTimescale;
#else	      
  tsfr = sqrt((All.PhysDensThresh * ThreshFact) / (SphP[i].d.Density * a3inv)) * All.MaxSfrTimescale;
#endif
#endif
#endif
  factorEVP = pow(SphP[i].d.Density * a3inv / (All.PhysDensThresh * ThreshFact), -0.8) * All.FactorEVP;

  egyhot = (All.EgySpecSN * EgySpecSNFact) / (1 + factorEVP) + (All.EgySpecCold * EgySpecColdFact);

  ne = SphP[i].Ne;
#ifdef METAL_COOLING
#ifdef PRIMODIAL_COOLING
  tcool = GetCoolingTime(egyhot, SphP[i].d.Density * a3inv, &ne, 0.0);
#else
  tcool = GetCoolingTime(egyhot, SphP[i].d.Density * a3inv, &ne, P[i].Metallicity);
#endif
#else
  tcool = GetCoolingTime(egyhot, SphP[i].d.Density * a3inv, &ne);
#endif

  y = tsfr / tcool * egyhot / (All.FactorSN * (All.EgySpecSN * EgySpecSNFact) 
			       - (1 - All.FactorSN) * (All.EgySpecCold * EgySpecColdFact));

  x = 1 + 1 / (2 * y) - sqrt(1 / y + 1 / (4 * y * y));
#if defined(STELLAR_FEEDBACK) || defined(SNII)
//#ifndef H2R
  cloudmass = P[i].Mass;
//#endif
#else
  cloudmass = x * P[i].Mass;
#endif
#endif

#if defined(STELLAR_FEEDBACK) || defined(SNII)
  rateOfSF = cloudmass / tsfr;
#else
  rateOfSF = (1 - All.FactorSN) * cloudmass / tsfr;
#endif
  /* convert to solar masses per yr */

  rateOfSF *= (All.UnitMass_in_g / SOLAR_MASS) / (All.UnitTime_in_s / SEC_PER_YEAR);

  return rateOfSF;
}

#endif /* closing of SFR-conditional */



#endif /* closes SFR_MHM conditional */





#if defined(SFR) || defined(BLACK_HOLES)
void rearrange_particle_sequence(void)
{
  int i, j, flag = 0, flag_sum;
  struct particle_data psave;

#ifdef BLACK_HOLES
  int count_elim, count_gaselim, tot_elim, tot_gaselim;
#endif

#ifdef SFR
  if(Stars_converted)
    {
      N_gas -= Stars_converted;
      Stars_converted = 0;

      for(i = 0; i < N_gas; i++)
	if(P[i].Type != 0)
	  {
	    for(j = N_gas; j < NumPart; j++)
	      if(P[j].Type == 0)
		break;

	    if(j >= NumPart)
	      endrun(181170);

	    psave = P[i];
	    P[i] = P[j];
	    SphP[i] = SphP[j];
	    P[j] = psave;
	  }
      flag = 1;
    }
#endif

#ifdef BLACK_HOLES
  count_elim = 0;
  count_gaselim = 0;

  for(i = 0; i < NumPart; i++)
    if(P[i].Mass == 0)
      {
	TimeBinCount[P[i].TimeBin]--;

	if(TimeBinActive[P[i].TimeBin])
	  NumForceUpdate--;

	if(P[i].Type == 0)
	  {
	    TimeBinCountSph[P[i].TimeBin]--;

	    P[i] = P[N_gas - 1];
	    SphP[i] = SphP[N_gas - 1];

	    P[N_gas - 1] = P[NumPart - 1];

	    N_gas--;

	    count_gaselim++;
	  }
	else
	  {
	    P[i] = P[NumPart - 1];
	  }

	NumPart--;
	i--;

	count_elim++;
      }

  MPI_Allreduce(&count_elim, &tot_elim, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
  MPI_Allreduce(&count_gaselim, &tot_gaselim, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);

  if(count_elim)
    flag = 1;

  if(ThisTask == 0)
    {
      printf("Blackholes: Eliminated %d gas particles and merged away %d black holes.\n",
	     tot_gaselim, tot_elim - tot_gaselim);
      fflush(stdout);
    }

  All.TotNumPart -= tot_elim;
  All.TotN_gas -= tot_gaselim;
  All.TotBHs -= tot_elim - tot_gaselim;
#endif

  MPI_Allreduce(&flag, &flag_sum, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);

  if(flag_sum)
    reconstruct_timebins();
}
#endif /* closing of SFR-conditional */



#if defined(SFR)
/* Initial Metallicity = 0 for initializing SF could (both init_clouds() and integrate_sfr()) */
void init_clouds(void)
{
  double A0, dens, tcool, ne, coolrate, egyhot, x, u4, meanweight;
  double tsfr, y, peff, fac, neff, egyeff, factorEVP, sigma, thresholdStarburst;

  if(All.PhysDensThresh == 0)
    {
#ifdef METAL_COOLING  /* Make the table for the PhysDensThresh(Z) */
#ifdef ADJUSTPHYSDENSTHRESH
      if(ThisTask == 0) printf("\nMaking PhysDensThresh(Z) table\n");

      int i;
      double Z, dlogZ, XHydro;
      /* The range of the tabel */
      double zz0 = 1.0E-5;  /* Recommanded to set zz0 = ZMin */
      double zz1 = 5.0E-2;

      dlogZ = (log10(zz1) - log10(zz0))/(NZtable-1);

      for(i = 0; i < NZtable; i++)
	{
	  logZtable[i] = log10(zz0) + dlogZ*i;
	  Z = pow(10.0, logZtable[i]);

	  A0 = All.FactorEVP;

	  egyhot = (All.EgySpecSN / meanweight_full_fact(Z)) / A0 
	    + (All.EgySpecCold / meanweight_neutral_fact(Z));

	  XHydro = 1 - Y_from_Z(Z) - Z;
	  ne = 0.0; 	    /* note: assuming ne = 0.0 since T=1.0e4 */
	  meanweight = 1.0 / (XHydro + Y_from_Z(Z) / 4.0 + Z / Mz_from_Z(Z) + ne * XHydro);
	  
	  u4 = 1 / meanweight * (1.0 / GAMMA_MINUS1) * (BOLTZMANN / PROTONMASS) * 1.0e4;
	  u4 *= All.UnitMass_in_g / All.UnitEnergy_in_cgs;
	  

	  if(All.ComovingIntegrationOn)
	    dens = 1.0e6 * 3 * All.Hubble * All.Hubble / (8 * M_PI * All.G);
	  else
	    dens = 1.0e6 * 3 * All.Hubble * All.Hubble / (8 * M_PI * All.G);
	  
	  if(All.ComovingIntegrationOn)
	    {
	      All.Time = 1.0;	/* to be guaranteed to get z=0 rate */
	      IonizeParams();
	    }
	  
	  ne = 1.0;
	  SetZeroIonization();
	  tcool = GetCoolingTime(egyhot, dens, &ne, Z);
	  coolrate = egyhot / tcool / dens;
	  
	  x = (egyhot - u4) / (egyhot - (All.EgySpecCold / meanweight_neutral_fact(Z)));
        
	  PhysDensThresh_Metal[i] =  x / pow(1 - x,2) * 
	    (All.FactorSN * (All.EgySpecSN / meanweight_full_fact(Z)) 
	     - (1 - All.FactorSN) * (All.EgySpecCold / meanweight_neutral_fact(Z))) /
	    (All.MaxSfrTimescale * coolrate);
	
	  if(ThisTask == 0) 
	    {
	      printf("Computed: PhysDensThresh= %g  (int units)         %g h^2 cm^-3 for Z=%g\n", PhysDensThresh_Metal[i],
		     PhysDensThresh_Metal[i] / (PROTONMASS / HYDROGEN_MASSFRAC / All.UnitDensity_in_cgs),Z);
	      printf("EXPECTED FRACTION OF COLD GAS AT THRESHOLD = %g\n", x);
	      printf("A0= %g tcool=%g dens=%g egyhot=%g\n",A0, tcool, dens, egyhot);
	    }
	}

      if(ThisTask == 0) printf("Finish making PhysDensThresh(Z) table\n");
#endif /* End ADJUSTPHYSDENSTHRESH */
#endif /* End making the PhysDensThresh(Z) table. Start compute the property for primodial mixture */

      A0 = All.FactorEVP;

      egyhot = All.EgySpecSN / A0 + All.EgySpecCold;

      meanweight = 4 / (8 - 5 * (1 - HYDROGEN_MASSFRAC));	/* note: assuming FULL ionization */

      u4 = 1 / meanweight * (1.0 / GAMMA_MINUS1) * (BOLTZMANN / PROTONMASS) * 1.0e4;
      u4 *= All.UnitMass_in_g / All.UnitEnergy_in_cgs;


      if(All.ComovingIntegrationOn)
	dens = 1.0e6 * 3 * All.Hubble * All.Hubble / (8 * M_PI * All.G);
      else
	dens = 1.0e6 * 3 * All.Hubble * All.Hubble / (8 * M_PI * All.G);

      if(All.ComovingIntegrationOn)
	{
	  All.Time = 1.0;	/* to be guaranteed to get z=0 rate */
	  IonizeParams();
	}

      ne = 1.0;
      SetZeroIonization();
#ifdef METAL_COOLING
      tcool = GetCoolingTime(egyhot, dens, &ne, 0.0);
#else
      tcool = GetCoolingTime(egyhot, dens, &ne);
#endif
      coolrate = egyhot / tcool / dens;

      x = (egyhot - u4) / (egyhot - All.EgySpecCold);

      All.PhysDensThresh =
	x / pow(1 - x,
		2) * (All.FactorSN * All.EgySpecSN - (1 -
						      All.FactorSN) * All.EgySpecCold) /
	(All.MaxSfrTimescale * coolrate);

#ifdef RESCALEPHYSDENSTHRESH
      All.PhysDensThresh *= All.ReScalePhysDensThresh;
#endif
      if(ThisTask == 0)
	{
	  printf("\nA0= %g  \n", A0);
	  printf("Computed: PhysDensThresh= %g  (int units)         %g h^2 cm^-3\n", All.PhysDensThresh,
		 All.PhysDensThresh / (PROTONMASS / HYDROGEN_MASSFRAC / All.UnitDensity_in_cgs));
	  printf("EXPECTED FRACTION OF COLD GAS AT THRESHOLD = %g\n\n", x);
	  printf("tcool=%g dens=%g egyhot=%g\n", tcool, dens, egyhot);
#ifdef RESCALEPHYSDENSTHRESH
	  printf("Note: PhysDensThresh increases %g times by rescaling\n", All.ReScalePhysDensThresh);
#endif	  
	}


#ifdef H2REGSF
      All.PhysDensThresh = (DENSTHRESH/(All.HubbleParam*All.HubbleParam)) * PROTONMASS / (HYDROGEN_MASSFRAC*All.UnitDensity_in_cgs);
      All.OTUVThresh     = ((DENSTHRESH*OTUVTHRESH)/(All.HubbleParam*All.HubbleParam)) * PROTONMASS / (HYDROGEN_MASSFRAC*All.UnitDensity_in_cgs);

      double pctocm =3.085678e18;
      double gtomsun=1.989e33;
      All.SIGMA_NORM = 1./((All.UnitMass_in_g/(All.UnitLength_in_cm*All.UnitLength_in_cm))*((pctocm*pctocm)/gtomsun));

      double s_to_gyr, G_internalunit, GyrInternal;
      s_to_gyr = 3.15569e16;
      G_internalunit = 6.674e-8 * All.UnitMass_in_g/(All.UnitLength_in_cm*All.UnitLength_in_cm*All.UnitLength_in_cm)*(s_to_gyr*s_to_gyr);
      GyrInternal    = 1./(0.98/All.HubbleParam);
      All.GInternal  = G_internalunit / (GyrInternal*GyrInternal);

      if(ThisTask == 0)
	{
	  printf("Setting PhysDensThresh=%g in pressure sfr (system unit)      %g h^2 cm^-3\n", All.PhysDensThresh,
		 All.PhysDensThresh / (PROTONMASS / HYDROGEN_MASSFRAC / All.UnitDensity_in_cgs));
	  printf("Setting OTUVThresh=%g (system unit)         %g h^2 cm^-3\n", All.OTUVThresh,
		 All.OTUVThresh / (PROTONMASS / HYDROGEN_MASSFRAC / All.UnitDensity_in_cgs));
	  printf("Setting SIGMA_NORM=%g (system unit)         \n", All.SIGMA_NORM);
	}

      //the following lines are from the SCHAYE_PRESSURE_MULTI model, used temporarily to allow for the calculation of egyhot
      /*
      double valA, valS;

      KS_SigmaThresh = All.SigmaThresh * (SOLAR_MASS/All.UnitMass_in_g) * pow(1.0e-6*CM_PER_MPC/All.UnitLength_in_cm,-2); 
      valA = KS_A * (SOLAR_MASS/All.UnitMass_in_g) * (All.UnitTime_in_s/SEC_PER_YEAR) * pow(1.0e-3*CM_PER_MPC/All.UnitLength_in_cm,-2);
      valS = KS_SIGMA0 * (SOLAR_MASS/All.UnitMass_in_g) * pow(1.0e-6*CM_PER_MPC/All.UnitLength_in_cm,-2);
      KS_Const = pow(valS,KS_n) / valA;     
      fg = 1.0;
      fth = 1.0;
      K_poly = KS_SigmaThresh * KS_SigmaThresh *(All.G/GAMMA) / pow(All.PhysDensThresh,All.GammaPoly) * fth /fg;
      */
#else //H2REGSF
#ifdef SCHAYE_PRESSURE_MULTI
      double valA, valS;
      /* convert physical unit to system unit */
      /* M_sun pc^-2 to system unit (KS_SigmaThresh) */
      KS_SigmaThresh = All.SigmaThresh * (SOLAR_MASS/All.UnitMass_in_g) * pow(1.0e-6*CM_PER_MPC/All.UnitLength_in_cm,-2); 
      /* M_sun yr^-1 kpc^-2 (KS_A) */
      valA = KS_A * (SOLAR_MASS/All.UnitMass_in_g) * (All.UnitTime_in_s/SEC_PER_YEAR) * pow(1.0e-3*CM_PER_MPC/All.UnitLength_in_cm,-2);
      /* M_sun pc^-2 (KS_SIGMA0) */
      valS = KS_SIGMA0 * (SOLAR_MASS/All.UnitMass_in_g) * pow(1.0e-6*CM_PER_MPC/All.UnitLength_in_cm,-2);

      /* compute the constant part in tsfr calculation */
      KS_Const = pow(valS,KS_n) / valA;

      /* Reevaluating PhysDensThresh according to KS_SigmaThresh 
       * Sigma = rho_g * Lj, Lj = cs/sqrt(G*(rho_t(=rho_g/fg))) where cs is sound speed (Schaye 2001).
       * rho_thresh = (G/fg) * (SigmaThresh/cs)^2
       * To evaluate cs we assume ideal gas with T=500.0 (cold dense cloud) 
       * We use polytropic EOS P_th = K_poly*rho^GammaPoly 
       * In this case, K_poly is evaluated as follow 
       * K_poly = KS_SigmaThresh * KS_SigmaThresh * (All.G/GAMMA) / pow(All.PhysDensThresh,All.GammaPoly) * fth / fg
       * Here we set u = u(T=500) and rho=All.PhysDensThresh.
       * Note1 : cs^2 = Gamma * P_tot/rho = Gamma*(Gamma-1)*u(T)/fth
       * Note2 : P = (Gamma-1)*rho*u = A * rho*GAMMA  where u(T) = k*T/(mu*mp*(Gamma-1))
       * Note3 : Unlike Schaye and Dalla Vecchia K_poly include the (1/fth) so that the pressure is thermal pressure.
       * Note4 : K_poly can also be evaluated as K_poly = (GammaPoly-1.)*u_cold*PhysDensThresh^(1-GammaPoly)
       * Note5 : approximated gas fraction of the disk; assumes f ~ fg ~ fth ~ 1
       * Note6 : f only effect determination of All.PhysDensThresh which also includes somewhat arbitrary set of T_cold and mu_cold
       *         its effect on tsfr is canceled.
       */

      /* fg = All.OmegaBaryon/All.Omega0 */       
      fg = 1.0;
      fth = 1.0;
      double mu_cold = 1.2 ; /* simply assume neutral cold gas */
      double T_cold = 500.0 ;  /* T=500 */
      double u_cold, cs;
      /* using GAMMA */
      u_cold = BOLTZMANN * T_cold / (PROTONMASS * mu_cold * GAMMA_MINUS1); 
      u_cold *= All.UnitMass_in_g / All.UnitEnergy_in_cgs; /** cgs to system unit **/
      cs = sqrt(u_cold * GAMMA_MINUS1 * GAMMA / fth);  
      All.PhysDensThresh = All.G * pow(KS_SigmaThresh/cs, 2) / fg;
      /* K_poly evaluation */
      K_poly = KS_SigmaThresh * KS_SigmaThresh *(All.G/GAMMA) / pow(All.PhysDensThresh,All.GammaPoly) * fth /fg;

      if(ThisTask == 0)
        {
          printf("Now tsfr is computed by pressure describtion with multiphase (Schaye & Dalla Vecchia 2008)\n");
	  printf("A=%g [M_sun yr^-1 kpc^-2] and %g in system unit\n",KS_A, valA);
	  printf("n=%g \n",KS_n);
	  printf("Input star formation threshold surface density is %g [M_sun pc^-2] and %g in system unit \n",All.SigmaThresh, KS_SigmaThresh);
	  printf("Re-computing PhysDensThresh=%g in pressure sfr (system unit)    %g h^2 cm^-3\n", All.PhysDensThresh,
		 All.PhysDensThresh / (PROTONMASS / HYDROGEN_MASSFRAC / All.UnitDensity_in_cgs));
	  printf("Polytropic eos for cold cloud : P = K*rho^Gamma_eff\n");
	  printf("where Gamma_eff = %g and K = %g\n",All.GammaPoly,K_poly );
        }
#endif //SCHAYE
#endif //H2REG

      dens = All.PhysDensThresh * 10; 

      do
	{
	  tsfr = sqrt(All.PhysDensThresh / (dens)) * All.MaxSfrTimescale;
	  factorEVP = pow(dens / All.PhysDensThresh, -0.8) * All.FactorEVP;
	  egyhot = All.EgySpecSN / (1 + factorEVP) + All.EgySpecCold;

	  ne = 0.5;
#ifdef METAL_COOLING
	  tcool = GetCoolingTime(egyhot, dens, &ne, 0.0);
#else
	  tcool = GetCoolingTime(egyhot, dens, &ne);
#endif

	  y = tsfr / tcool * egyhot / (All.FactorSN * All.EgySpecSN - (1 - All.FactorSN) * All.EgySpecCold);
	  x = 1 + 1 / (2 * y) - sqrt(1 / y + 1 / (4 * y * y));
	  egyeff = egyhot * (1 - x) + All.EgySpecCold * x;

	  peff = GAMMA_MINUS1 * dens * egyeff;

	  fac = 1 / (log(dens * 1.025) - log(dens));
	  dens *= 1.025;

	  neff = -log(peff) * fac;

	  tsfr = sqrt(All.PhysDensThresh / (dens)) * All.MaxSfrTimescale;
	  factorEVP = pow(dens / All.PhysDensThresh, -0.8) * All.FactorEVP;
	  egyhot = All.EgySpecSN / (1 + factorEVP) + All.EgySpecCold;

	  ne = 0.5;
#ifdef METAL_COOLING
	  tcool = GetCoolingTime(egyhot, dens, &ne, 0.0);
#else
	  tcool = GetCoolingTime(egyhot, dens, &ne);
#endif

	  y = tsfr / tcool * egyhot / (All.FactorSN * All.EgySpecSN - (1 - All.FactorSN) * All.EgySpecCold);
	  x = 1 + 1 / (2 * y) - sqrt(1 / y + 1 / (4 * y * y));
	  egyeff = egyhot * (1 - x) + All.EgySpecCold * x;

	  peff = GAMMA_MINUS1 * dens * egyeff;

	  neff += log(peff) * fac;
	}
      while(neff > 4.0 / 3);

      thresholdStarburst = dens;

#ifdef MODIFIEDBONDI
      All.BlackHoleRefDensity = thresholdStarburst;
      All.BlackHoleRefSoundspeed = sqrt(GAMMA * GAMMA_MINUS1 * egyeff);
#endif


      if(ThisTask == 0)
	{
	  printf("Run-away sets in for dens=%g\n", thresholdStarburst);
	  printf("Dynamic range for quiescent star formation= %g\n", thresholdStarburst / All.PhysDensThresh);
	  fflush(stdout);
	}

#if defined(SCHAYE_PRESSURE_MULTI) || defined(H2REGSF) || defined(STELLAR_FEEDBACK) || defined(SNII)
//#ifdef SCHAYE_PRESSURE_MULTI 
      if(ThisTask == 0) 
	printf("Now, we do not generate eos.txt and sfrrate.txt for pressure SFR ....\n");
#else      
      integrate_sfr();
#endif

      if(ThisTask == 0)
	{
	  sigma = 10.0 / All.Hubble * 1.0e-10 / pow(1.0e-3, 2);

	  printf("Isotherm sheet central density: %g   z0=%g\n",
		 M_PI * All.G * sigma * sigma / (2 * GAMMA_MINUS1) / u4,
		 GAMMA_MINUS1 * u4 / (2 * M_PI * All.G * sigma));
	  fflush(stdout);

	}

      if(All.ComovingIntegrationOn)
	{
	  All.Time = All.TimeBegin;
	  IonizeParams();
	}

#ifdef WINDS
#ifndef VWINDS
      if(All.WindEfficiency > 0)
	if(ThisTask == 0)
	  printf("Windspeed: %g\n",
		 sqrt(2 * All.WindEnergyFraction * All.FactorSN * All.EgySpecSN / (1 - All.FactorSN) /
		      All.WindEfficiency));
#endif
#endif
    }
}

void integrate_sfr(void)
{
  double rho0, rho, rho2, q, dz, gam, sigma = 0, sigma_u4, sigmasfr = 0, ne, P1;
  double x = 0, y, P, P2, x2, y2, tsfr2, factorEVP2, egyhot2, tcool2, drho, dq;
  double meanweight, u4, z, tsfr, tcool, egyhot, factorEVP, egyeff, egyeff2;
  FILE *fd;


  meanweight = 4 / (8 - 5 * (1 - HYDROGEN_MASSFRAC));	/* note: assuming FULL ionization */
  u4 = 1 / meanweight * (1.0 / GAMMA_MINUS1) * (BOLTZMANN / PROTONMASS) * 1.0e4;
  u4 *= All.UnitMass_in_g / All.UnitEnergy_in_cgs;

  if(All.ComovingIntegrationOn)
    {
      All.Time = 1.0;		/* to be guaranteed to get z=0 rate */
      IonizeParams();
    }

  if(ThisTask == 0)
    fd = fopen("eos.txt", "w");
  else
    fd = 0;

  for(rho = All.PhysDensThresh; rho <= 1000 * All.PhysDensThresh; rho *= 1.1)
    {
      tsfr = sqrt(All.PhysDensThresh / rho) * All.MaxSfrTimescale;

      factorEVP = pow(rho / All.PhysDensThresh, -0.8) * All.FactorEVP;

      egyhot = All.EgySpecSN / (1 + factorEVP) + All.EgySpecCold;

      ne = 1.0;
#ifdef METAL_COOLING
      tcool = GetCoolingTime(egyhot, rho, &ne, 0.0);
#else
      tcool = GetCoolingTime(egyhot, rho, &ne);
#endif

      y = tsfr / tcool * egyhot / (All.FactorSN * All.EgySpecSN - (1 - All.FactorSN) * All.EgySpecCold);
      x = 1 + 1 / (2 * y) - sqrt(1 / y + 1 / (4 * y * y));

      egyeff = egyhot * (1 - x) + All.EgySpecCold * x;

      P = GAMMA_MINUS1 * rho * egyeff;

      if(ThisTask == 0)
	{
	  fprintf(fd, "%g %g\n", rho, P);
	}
    }

  if(ThisTask == 0)
    fclose(fd);


  if(ThisTask == 0)
    fd = fopen("sfrrate.txt", "w");
  else
    fd = 0;

  for(rho0 = All.PhysDensThresh; rho0 <= 10000 * All.PhysDensThresh; rho0 *= 1.02)
    {
      z = 0;
      rho = rho0;
      q = 0;
      dz = 0.001;

      sigma = sigmasfr = sigma_u4 = 0;

      while(rho > 0.0001 * rho0)
	{
	  if(rho > All.PhysDensThresh)
	    {
	      tsfr = sqrt(All.PhysDensThresh / rho) * All.MaxSfrTimescale;

	      factorEVP = pow(rho / All.PhysDensThresh, -0.8) * All.FactorEVP;

	      egyhot = All.EgySpecSN / (1 + factorEVP) + All.EgySpecCold;

	      ne = 1.0;
#ifdef METAL_COOLING
	      tcool = GetCoolingTime(egyhot, rho, &ne, 0.0);
#else
	      tcool = GetCoolingTime(egyhot, rho, &ne);
#endif

	      y =
		tsfr / tcool * egyhot / (All.FactorSN * All.EgySpecSN - (1 - All.FactorSN) * All.EgySpecCold);
	      x = 1 + 1 / (2 * y) - sqrt(1 / y + 1 / (4 * y * y));

	      egyeff = egyhot * (1 - x) + All.EgySpecCold * x;

	      P = P1 = GAMMA_MINUS1 * rho * egyeff;

	      rho2 = 1.1 * rho;
	      tsfr2 = sqrt(All.PhysDensThresh / rho2) * All.MaxSfrTimescale;
	      factorEVP2 = pow(rho2 / All.PhysDensThresh, -0.8) * All.FactorEVP;
	      egyhot2 = All.EgySpecSN / (1 + factorEVP) + All.EgySpecCold;
#ifdef METAL_COOLING
	      tcool2 = GetCoolingTime(egyhot2, rho2, &ne, 0.0);
#else
	      tcool2 = GetCoolingTime(egyhot2, rho2, &ne);
#endif
	      y2 =
		tsfr2 / tcool2 * egyhot2 / (All.FactorSN * All.EgySpecSN -
					    (1 - All.FactorSN) * All.EgySpecCold);
	      x2 = 1 + 1 / (2 * y2) - sqrt(1 / y2 + 1 / (4 * y2 * y2));
	      egyeff2 = egyhot2 * (1 - x2) + All.EgySpecCold * x2;
	      P2 = GAMMA_MINUS1 * rho2 * egyeff2;

	      gam = log(P2 / P1) / log(rho2 / rho);
	    }
	  else
	    {
	      tsfr = 0;

	      P = GAMMA_MINUS1 * rho * u4;
	      gam = 1.0;


	      sigma_u4 += rho * dz;
	    }



	  drho = q;
	  dq = -(gam - 2) / rho * q * q - 4 * M_PI * All.G / (gam * P) * rho * rho * rho;

	  sigma += rho * dz;
	  if(tsfr > 0)
	    {
	      sigmasfr += (1 - All.FactorSN) * rho * x / tsfr * dz;
	    }

	  rho += drho * dz;
	  q += dq * dz;
	}


      sigma *= 2;		/* to include the other side */
      sigmasfr *= 2;
      sigma_u4 *= 2;


      if(ThisTask == 0)
	{
	  fprintf(fd, "%g %g %g %g\n", rho0, sigma, sigmasfr, sigma_u4);
	}
    }


  if(All.ComovingIntegrationOn)
    {
      All.Time = All.TimeBegin;
      IonizeParams();
    }

  if(ThisTask == 0)
    fclose(fd);
}

#endif /* closing of SFR-conditional */


#if defined(SFR)
void set_units_sfr(void)
{
  double meanweight;

#ifdef COSMIC_RAYS
  double feedbackenergyinergs;
#endif

  All.OverDensThresh =
    All.CritOverDensity * All.OmegaBaryon * 3 * All.Hubble * All.Hubble / (8 * M_PI * All.G);

  All.PhysDensThresh = All.CritPhysDensity * PROTONMASS / HYDROGEN_MASSFRAC / All.UnitDensity_in_cgs;

  meanweight = 4 / (1 + 3 * HYDROGEN_MASSFRAC);	/* note: assuming NEUTRAL GAS */

  All.EgySpecCold = 1 / meanweight * (1.0 / GAMMA_MINUS1) * (BOLTZMANN / PROTONMASS) * All.TempClouds;
  All.EgySpecCold *= All.UnitMass_in_g / All.UnitEnergy_in_cgs;

  meanweight = 4 / (8 - 5 * (1 - HYDROGEN_MASSFRAC));	/* note: assuming FULL ionization */

  All.EgySpecSN = 1 / meanweight * (1.0 / GAMMA_MINUS1) * (BOLTZMANN / PROTONMASS) * All.TempSupernova;
  All.EgySpecSN *= All.UnitMass_in_g / All.UnitEnergy_in_cgs;


#ifdef COSMIC_RAYS
  if(All.CR_SNEff < 0.0)
    /* if CR_SNeff < 0.0, then substract CR Feedback energy from thermal
     * feedback energy
     */
    {
      if(ThisTask == 0)
	{
	  printf("%g percent of thermal feedback go into Cosmic Rays.\nRemaining ", -100.0 * All.CR_SNEff);
	}

      All.EgySpecSN *= (1.0 + All.CR_SNEff);
      All.CR_SNEff = -All.CR_SNEff / (1.0 + All.CR_SNEff);

    }

  All.FeedbackEnergy = All.FactorSN / (1 - All.FactorSN) * All.EgySpecSN;

  feedbackenergyinergs = All.FeedbackEnergy / All.UnitMass_in_g * (All.UnitEnergy_in_cgs * SOLAR_MASS);

  if(ThisTask == 0)
    {
      printf("Feedback energy per formed solar mass in stars= %g  ergs\n", feedbackenergyinergs);
      printf("OverDensThresh= %g\nPhysDensThresh= %g (internal units)\n", All.OverDensThresh,
	     All.PhysDensThresh);
    }

#endif
}

#endif /* closes SFR */

#endif /* closes COOLING */
