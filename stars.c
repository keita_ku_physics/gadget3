#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <gsl/gsl_math.h>

#include "allvars.h"
#include "proto.h"

#ifdef SN_FEEDBACK

//10-2-13 RT: add CPU_STARS to the timing

/* "static" means that the structure is only used here and not passed outside
 * of this program */

static struct stardata_in
{
  MyDouble Pos[3];
  MyFloat Density;
  MyFloat Dt;
  MyFloat Hsml;
  MyFloat Mass;
  MyFloat Vel[3];
  MyFloat Csnd;
  MyIDType ID;
    int Index;
    int NodeList[NODELISTLENGTH];

#ifdef METALS
  MyFloat Metallicity;
#endif

#ifdef LIFETIMES
  MyFloat n_per_tstep;
#endif

#ifdef TYPE1A
  MyFloat StellarAge;
  MyFloat SN1a_num;
#endif

#ifdef STELLAR_MASS_LOSS
#ifdef LIFETIMES
  MyFloat return_mass;
#ifdef METALS
#ifdef TRACK_O_FE
  MyFloat mFe, mO;
#endif
#endif
#endif
#endif
}
*StarDataIn, *StarDataGet; /* These are things passed between processors */

static struct stardata_out
{
  MyLongDouble Mass;
}
*StarDataResult, *StarDataOut; /* These are structures local to the
					  * processor */

static double hubble_a, ascale;

void stellar_feedback(void)
{
  int i, j, k, n;
  int ndone_flag, ndone;
  int ngrp, sendTask, recvTask, place, nexport, nimport, dummy;
  double dt;
  MPI_Status status;

  double fracSN,tms;

#ifdef TYPE1A
  double fracSN1a, tms1a, delayt1a;
#endif

  // Quickly calculate some numbers that we need for the SNe feedback
  
  // What is the mean age of stars that can go supernova? Assume all stars
  // go pop at this point!

#ifndef KroupaIMF  // In other words, a Salpeter IMF
  fracSN = (pow(All.StellarMassMax,2.0-All.SlopeIMF)-pow(8.0,2.0-All.SlopeIMF))/
    (pow(All.StellarMassMax,2.0-All.SlopeIMF)-pow(All.StellarMassMin,2.0-All.SlopeIMF));

  tms = 1.0e4 * (2.0-All.SlopeIMF)/(-0.5-All.SlopeIMF) * 
    (pow(All.StellarMassMax,-0.5-All.SlopeIMF)-pow(8.0,-0.5-All.SlopeIMF))/
    (pow(All.StellarMassMax, 2.0-All.SlopeIMF)-pow(8.0, 2.0-All.SlopeIMF));

  tms = tms/All.UnitTime_in_Megayears;
#endif

#ifdef CHABRIER_IMF //use a Chabrier IMF
  //Above 1 solar mass
  fracSN = 0.011; //number of SNeII per solar mass for Chabrier IMF
#endif

#ifdef TYPE1A
  fracSN1a = 0.0013; //number of SNe 1a per solar mass (from Maoz et al. 2012)
  tms1a = 10.0*(1000./All.UnitTime_in_Megayears); //in Gyr
#endif

#ifdef PADOVA_LIFETIMES
  //tba
#endif

  if(All.ComovingIntegrationOn)
    {
      ascale = All.Time;
      hubble_a = hubble_function(All.Time);
    }
  else
    hubble_a = ascale = 1;
  
  if(ThisTask == 0)
    {
      printf("Starting stellar feedback...\n");
      fflush(stdout);
    }
  
  /* allocate buffers to arrange communication */

  Ngblist = (int *) mymalloc(NumPart * sizeof(int));

  All.BunchSize =
    (int) ((All.BufferSize * 1024 * 1024) / (sizeof(struct data_index) + sizeof(struct data_nodelist) +
					     sizeof(struct stardata_in) +
					     sizeof(struct stardata_out) +
					     sizemax(sizeof(struct stardata_in),
						     sizeof(struct stardata_out))));
  DataIndexTable = (struct data_index *) mymalloc(All.BunchSize * sizeof(struct data_index));
  DataNodeList = (struct data_nodelist *) mymalloc(All.BunchSize * sizeof(struct data_nodelist));

  /** Let's first spread the feedback energy, and determine which particles may be swalled by whom */

  i = FirstActiveParticle;	/* first particle for this task */

  do
    {
      for(j = 0; j < NTask; j++)
	{
	  Send_count[j] = 0;
	  Exportflag[j] = -1;
	}
      
      for(nexport = 0; i >= 0; i = NextActiveParticle[i]) {
#ifndef LIFETIMES
	if(P[i].Type == 4 && P[i].StellarActive == 1 && ((All.Time-P[i].StellarAge)/hubble_a)>=tms)
#else
	if(P[i].Type == 4 && P[i].StellarActive == 1 && P[i].Mass > (1.0-RETURN_FRAC)*P[i].InitialMass) //with lifetimes don't want to use a fixed tms, feedback is going on all the time
#endif
	  {
	    if(star_evaluate(i, 0, &nexport, Send_count) < 0)
	      break;
	  }
      }

      /* Rearrange export Table to minimize number of MPI communications */
      
#ifdef MYSORT
      mysort_dataindex(DataIndexTable, nexport, sizeof(struct data_index), data_index_compare);
#else
      qsort(DataIndexTable, nexport, sizeof(struct data_index), data_index_compare);
#endif

      /* Pass Export Table between processes */
      MPI_Allgather(Send_count, NTask, MPI_INT, Sendcount_matrix, NTask, MPI_INT, MPI_COMM_WORLD);
      
      for(j = 0, nimport = 0, Recv_offset[0] = 0, Send_offset[0] = 0; j < NTask; j++)
	{
	  Recv_count[j] = Sendcount_matrix[j * NTask + ThisTask];
	  nimport += Recv_count[j];
	  
	  if(j > 0)
	    {
	      Send_offset[j] = Send_offset[j - 1] + Send_count[j - 1];
	      Recv_offset[j] = Recv_offset[j - 1] + Recv_count[j - 1];
	    }
	}
      
      StarDataGet = (struct stardata_in *) mymalloc(nimport * sizeof(struct stardata_in));
      StarDataIn = (struct stardata_in *) mymalloc(nexport * sizeof(struct stardata_in));

      for(j = 0; j < nexport; j++)
	{
	  place = DataIndexTable[j].Index;
	  
	  for(k = 0; k < 3; k++)
	    {
	      StarDataIn[j].Pos[k] = P[place].Pos[k];
	      StarDataIn[j].Vel[k] = P[place].Vel[k];
	    }
	  
	  StarDataIn[j].Hsml = PPP[place].Hsml;
	  StarDataIn[j].Mass = P[place].Mass;
	  StarDataIn[j].ID = P[place].ID;

#ifdef METALS
	  StarDataIn[j].Metallicity = P[place].Metallicity;
#endif

#ifdef LIFETIMES
	  StarDataIn[j].n_per_tstep = P[place].n_per_tstep;
#endif

#ifdef TYPE1A
	  StarDataIn[j].StellarAge = P[place].StellarAge;
	  StarDataIn[j].SN1a_num = P[place].SN1a_num;
#endif

#ifdef STELLAR_MASS_LOSS
#ifdef LIFETIMES
	  StarDataIn[j].return_mass = P[place].return_mass;
#ifdef METALS
#ifdef TRACK_O_FE
	  StarDataIn[j].mFe = P[place].mFe;
	  StarDataIn[j].mO = P[place].mO;
#endif
#endif
#endif
#endif
	  memcpy(StarDataIn[j].NodeList,
		 DataNodeList[DataIndexTable[j].IndexGet].NodeList, NODELISTLENGTH * sizeof(int));
	}

      for(ngrp = 1; ngrp < (1 << PTask); ngrp++)
	{
	  sendTask = ThisTask;
	  recvTask = ThisTask ^ ngrp;
	  
	  if(recvTask < NTask)
	    {
	      if(Send_count[recvTask] > 0 || Recv_count[recvTask] > 0)
		{
		  /* get the particles */
		  MPI_Sendrecv(&StarDataIn[Send_offset[recvTask]],
			       Send_count[recvTask] * sizeof(struct stardata_in), MPI_BYTE,
			       recvTask, TAG_DENS_A,
			       &StarDataGet[Recv_offset[recvTask]],
			       Recv_count[recvTask] * sizeof(struct stardata_in), MPI_BYTE,
			       recvTask, TAG_DENS_A, MPI_COMM_WORLD, &status);
		}
	    }
	}

      /* free up memory now that the data were passed around*/
      myfree(StarDataIn);

      StarDataResult = (struct stardata_out *) mymalloc(nimport * sizeof(struct stardata_out));
      StarDataOut = (struct stardata_out *) mymalloc(nexport * sizeof(struct stardata_out));

      /* now consider the star particles that were sent to us. Thus mode=1 below */

      for(j = 0; j < nimport; j++)
	star_evaluate(j, 1, &dummy, &dummy);
      /* This has determined which non-local stars will accrete which local SPH particles */

      if(i < 0)
	ndone_flag = 1;
      else
	ndone_flag = 0;

      MPI_Allreduce(&ndone_flag, &ndone, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);

      /* Now return the results into the processors where the exported stars reside */
      for(ngrp = 1; ngrp < (1 << PTask); ngrp++)
	{
	  sendTask = ThisTask;
	  recvTask = ThisTask ^ ngrp;
	  if(recvTask < NTask)
	    {
	      if(Send_count[recvTask] > 0 || Recv_count[recvTask] > 0)
		{
		  /* send the results */
		  MPI_Sendrecv(&StarDataResult[Recv_offset[recvTask]],
			       Recv_count[recvTask] * sizeof(struct stardata_out),
			       MPI_BYTE, recvTask, TAG_DENS_B,
			       &StarDataOut[Send_offset[recvTask]],
			       Send_count[recvTask] * sizeof(struct stardata_out),
			       MPI_BYTE, recvTask, TAG_DENS_B, MPI_COMM_WORLD, &status);
		}
	    }

	}

      /* add the result to the particles */
      for(j = 0; j < nexport; j++)
	{
	  place = DataIndexTable[j].Index;
	}

      myfree(StarDataOut);
      myfree(StarDataResult);
      myfree(StarDataGet);
    }
  while(ndone < NTask);

  myfree(DataNodeList);
  myfree(DataIndexTable);
  myfree(Ngblist);

  for(n = FirstActiveParticle; n >= 0; n = NextActiveParticle[n])
#ifdef LIFETIMES
#if defined(SN_FEEDBACK) && defined(SN_MASS_INJECT)
    if(P[n].Type == 4 && P[n].StellarActive == 1)
      {
	P[n].Mass -= All.MassFeedbackFactor * (P[n].mFe_inject + P[n].mO_inject);
      }
#endif
#ifdef STELLAR_MASS_LOSS
  if(P[n].Type == 4 && P[n].StellarActive == 1 && P[n].Mass > (1.0-RETURN_FRAC)*P[n].InitialMass)
      {
	P[n].Mass -= All.MassFeedbackFactor * P[n].return_mass;
      }
#endif

#else
#ifdef LIFETIMES
    if(P[n].Type == 4 && P[n].StellarActive == 1 && P[i].Mass <= (1.0-RETURN_FRAC)*P[i].InitialMass)
#else
    if(P[n].Type == 4 && P[n].StellarActive == 1 && ((All.Time-P[n].StellarAge)/hubble_a)>=tms)
#endif
      {
	P[n].StellarActive = 0;  // Ensure that star particle is no longer
	                         // producing feedback.
      }

  if(ThisTask == 0)
    {
      printf("Stellar feedback finished...\n");
      fflush(stdout);
    }
#endif

#ifdef TYPE1A
  if(P[n].Type == 4 && P[n].SN1A_Active == 1 && ((All.Time-P[n].StellarAge)/hubble_a)>=tms1a)
    {
      P[n].SN1A_Active = 0;  // Ensure that star particle is no longer
                             // producing SN1a feedback.	
    }  
#endif

  //CPU_Step[CPU_STARS] += measure_time();
  CPU_Step[CPU_MISC] += measure_time();
}

int star_evaluate(int target, int mode, int *nexport, int *nSend_local)
{
  int startnode, numngb, j, k, n, index, id, listindex = 0;
  MyFloat *pos, *velocity, h_i, dt, mdot, rho, mass, bh_mass, csnd;
  double dx, dy, dz, h_i2, r2, r, u, hinv, hinv3, hinv4, wk, dwk, vrel;
  double energy;
  double fracSN,tms;

#ifdef LIFETIMES
  MyFloat n_per_tstep;
#endif

#ifdef TYPE1A
  double fracSN1a;
  MyFloat stellarage;
  MyFloat SN1a_num;
#endif

  double mZ;

#ifdef METALS
  double metallicity;
  double mass_inject;
#ifdef TRACK_O_FE
  //double mZ;
  double frac_mFe, frac_mO;
  double mFe_inject;
  double mO_inject;
#ifdef SN_MASS_INJECT
  double sn_mass;
#endif
#ifdef TYPE1A
  double mFe_SN1a;
  double mO_SN1a;
  double SN1a_mFe_inject;
  double SN1a_mO_inject;
#endif
#else
  double metals_inject;
#endif
#endif

#ifdef STELLAR_MASS_LOSS
#ifdef LIFETIMES
  double mass_loss;
  double return_mass;
#ifdef METALS
#ifdef TRACK_O_FE
  double mFe, mO;
  double ratio_mFe, ratio_mO;
  double mFe_loss, mO_loss;
#endif
#endif
#endif
#endif

// Quickly calculate some numbers that we need for the SNe feedback  
// What fraction of the stellar mass is available to go supernova?
#ifndef KroupaIMF  // In other words, a Salpeter IMF
  fracSN = (pow(All.StellarMassMax,2.0-All.SlopeIMF)-pow(8.0,2.0-All.SlopeIMF))/(pow(All.StellarMassMax,2.0-All.SlopeIMF)-pow(All.StellarMassMin,2.0-All.SlopeIMF));
#endif
  
#ifdef CHABRIER_IMF //use a Chabrier IMF
  fracSN = 0.011; //number of SNe per solar mass for Chabrier IMF
#endif

#ifdef TRACK_O_FE
  frac_mFe = 0.0011; //fraction of total stellar mass ejected as Fe for SNII
  frac_mO = 0.0133; //fraction of total stellar mass ejected as O for SNII
#endif

#ifdef TYPE1A
  fracSN1a = 0.0013; //number of SNe 1a per solar mass (from Maoz et al. 2012)
#ifdef TRACK_O_FE  
  mFe_SN1a = 0.63; //in solar masses
  mO_SN1a = 0.14; //in solar masses
#endif
#endif

  if(mode == 0)
    {
      pos = P[target].Pos;
      dt = (P[target].TimeBin ? (1 << P[target].TimeBin) : 0) * All.Timebase_interval / hubble_a;
      h_i = PPP[target].Hsml;
      mass = P[target].Mass;
      velocity = P[target].Vel;
      index = target;
      id = P[target].ID;
#ifdef METALS
      metallicity = P[target].Metallicity;
#endif
#ifdef LIFETIMES
      n_per_tstep = P[target].n_per_tstep;
#endif
#ifdef TYPE1A
      stellarage = P[target].StellarAge;
      SN1a_num = P[target].SN1a_num;
#endif
#ifdef STELLAR_MASS_LOSS
#ifdef LIFETIMES
      return_mass = P[target].return_mass;
#ifdef METALS
#ifdef TRACK_O_FE
      mFe = P[target].mFe;
      mO = P[target].mO;
#endif
#endif
#endif
#endif
    }
  else
    {
      pos = StarDataGet[target].Pos;
      dt = StarDataGet[target].Dt;
      h_i = StarDataGet[target].Hsml;
      mass = StarDataGet[target].Mass;
      velocity = StarDataGet[target].Vel;
      index = StarDataGet[target].Index;
      id = StarDataGet[target].ID;
#ifdef METALS
      metallicity = StarDataGet[target].Metallicity;
#endif
#ifdef LIFETIMES
      n_per_tstep = StarDataGet[target].n_per_tstep;
#endif
#ifdef TYPE1A
      stellarage = StarDataGet[target].StellarAge;
      SN1a_num = StarDataGet[target].SN1a_num;
#endif
#ifdef STELLAR_MASS_LOSS
#ifdef LIFETIMES
      return_mass = StarDataGet[target].return_mass;
#ifdef METALS
#ifdef TRACK_O_FE
      mFe = StarDataGet[target].mFe;
      mO = StarDataGet[target].mO;
#endif
#endif
#endif
#endif
    }

  /* initialize variables before SPH loop is started */
  h_i2 = h_i * h_i;
  
  /* Now start the actual SPH computation for this particle */
  if(mode == 0)
    {
      startnode = All.MaxPart;	/* root node */
    }
  else
    {
      startnode = StarDataGet[target].NodeList[0];
      startnode = Nodes[startnode].u.d.nextnode;	/* open it */
    }

  while(startnode >= 0)
    {
      while(startnode >= 0)
	{
	  numngb = ngb_treefind_star(pos, h_i, target, &startnode, mode, nexport, nSend_local);

	  if(numngb < 0)
	    return -1;
	  
	  for(n = 0; n < numngb; n++)
	    {
	      j = Ngblist[n];

	      if(P[j].Mass > 0)
		{
		  if(mass > 0)
		    {
		      dx = pos[0] - P[j].Pos[0];
		      dy = pos[1] - P[j].Pos[1];
		      dz = pos[2] - P[j].Pos[2];
#ifdef PERIODIC			/*  now find the closest image in the given box size  */
		      if(dx > boxHalf_X)
			dx -= boxSize_X;
		      if(dx < -boxHalf_X)
			dx += boxSize_X;
		      if(dy > boxHalf_Y)
			dy -= boxSize_Y;
		      if(dy < -boxHalf_Y)
			dy += boxSize_Y;
		      if(dz > boxHalf_Z)
			dz -= boxSize_Z;
		      if(dz < -boxHalf_Z)
			dz += boxSize_Z;
#endif // PERIODIC	
		      r2 = dx * dx + dy * dy + dz * dz;

		      if(r2 < h_i2)
			{
			  if(P[j].Type == 0)
			    {
			      /* here we have a gas particle */

			      r = sqrt(r2);
			      hinv = 1 / h_i;
			      hinv3 = hinv * hinv * hinv;
			      hinv4 = hinv3 * hinv;
			      u = r * hinv;
			      
			      /*
			      if(u < 0.5)
				wk = hinv3 * (KERNEL_COEFF_1 + KERNEL_COEFF_2 * (u - 1) * u * u);
			      else
				wk = hinv3 * KERNEL_COEFF_5 * (1.0 - u) * (1.0 - u) * (1.0 - u);
			      */
			      kernel_main(u, hinv3, hinv4, &wk, &dwk, 0);

			      if(P[j].Mass > 0)
				{
#ifdef LIFETIMES
				  energy = All.StellarFeedbackFactor * (1.e51/All.UnitEnergy_in_cgs) * n_per_tstep * mass; //n_per_tstep is number of individual stars that have gone supernovae in this timestep, per unit mass
#else
				  energy = All.StellarFeedbackFactor * (1.e51/All.UnitEnergy_in_cgs) * fracSN * mass *(All.UnitMass_in_g / SOLAR_MASS); //using single tms
#endif
				  SphP[j].ii.dInjected_SN_Energy += FLT(energy * P[j].Mass * wk / SphP[j].d.Density);

#ifdef METALS
#ifdef TRACK_O_FE
				  mZ = 2.09*frac_mO*mass + 1.06*frac_mFe*mass; //total metal mass from masses of Oxygen and Iron

				  /* Definition of metallicity is M_O_FE/M_total for each gas particle
				     So we simply track the mFe and mO for each gas particle and do the
				     ratio when we need to get Z */
#ifdef LIFETIMES
				  /* frac_mFe is already integrated over the IMF, simply use n_per_tstep to get the ratio 
				     for the number in this TIMESTEP out of the total number that go SNe for the whole IMF */
				  mFe_inject = All.MetalsFeedbackFactor * (frac_mFe * mass) * (n_per_tstep/(CHABRIER_SNII_TOTAL*All.UnitMass_in_g));
				  mO_inject = All.MetalsFeedbackFactor * (frac_mO * mass) * (n_per_tstep/(CHABRIER_SNII_TOTAL*All.UnitMass_in_g)); 
#else
				  mFe_inject = All.MetalsFeedbackFactor * frac_mFe * mass;
				  mO_inject = All.MetalsFeedbackFactor * frac_mO * mass;
#endif
				  SphP[j].iif.dInjected_SN_mFe += FLT(mFe_inject * P[j].Mass * wk / SphP[j].d.Density);
				  SphP[j].iio.dInjected_SN_mO += FLT(mO_inject * P[j].Mass * wk / SphP[j].d.Density);

#ifdef SN_MASS_INJECT
				  /* We use the information about the metal mass injected to also inject 'mass' */
				  sn_mass = All.MassFeedbackFactor * (mFe_inject + mO_inject);
				  SphP[j].iism.dInjected_SN_Mass += FLT(sn_mass * P[j].Mass * wk / SphP[j].d.Density);
#endif



#ifdef TYPE1A
				  if(((All.Time-stellarage)/hubble_a) > 0.1*(1000./All.UnitTime_in_Megayears) && ((All.Time-stellarage)/hubble_a) < 10.0*(1000./All.UnitTime_in_Megayears)) //in code units (Gyr)
				    {
				      SN1a_mFe_inject = All.MetalsFeedbackFactor * (mFe_SN1a*SOLAR_MASS/All.UnitMass_in_g)*SN1a_num*mass; //in code units
				      SN1a_mO_inject = All.MetalsFeedbackFactor * (mO_SN1a*SOLAR_MASS/All.UnitMass_in_g)*SN1a_num*mass; //in code units
				      SphP[j].iaf.dInjected_SN1a_mFe += FLT(SN1a_mFe_inject * P[j].Mass * wk / SphP[j].d.Density);
				      SphP[j].iao.dInjected_SN1a_mO += FLT(SN1a_mO_inject * P[j].Mass * wk / SphP[j].d.Density);
				    }
#endif

#else
				  mZ = 0.1*mass; //currently arbitrary, change this later
				  metals_inject = All.MetalsFeedbackFactor * mZ + 0.1; //change this later
				  SphP[j].iii.dInjected_SN_Metallicity += FLT(metals_inject * P[j].Mass * wk / SphP[j].d.Density);
#endif //TRACK_O_FE
	
#endif //METALS

#ifdef STELLAR_MASS_LOSS
#ifdef LIFETIMES
				  mass_loss = All.MassFeedbackFactor * return_mass; //return_mass calculated in timestep.c
#ifdef METALS
#ifdef TRACK_O_FE
				  /* The mass that is returned needs to have the same metallicity as the star particle that is returning it.
				     However, with TRACK_O_FE, the metallicity is decided based upon the proportion of O and Fe masses. We
				     therefore need to make sure that the values of P[i].mFe and P[i].mO (and P[i].mZ) are updated in
				     accordance with how much mass has been returned from a particle with a given metallicity */
				  ratio_mFe = mFe/mass; //the star particle composition will have been inherited from the gas particle progenitor
				  ratio_mO = mO/mass; //the star particle composition will have been inherited from the gas particle progenitor
				  mFe_loss = All.MassFeedbackFactor * ratio_mFe*return_mass;
				  mO_loss = All.MassFeedbackFactor * ratio_mO*return_mass;

				  SphP[j].iimf.dInjected_W_mFe += FLT(mFe_loss * P[j].Mass * wk / SphP[j].d.Density);
				  SphP[j].iimo.dInjected_W_mO += FLT(mO_loss * P[j].Mass * wk / SphP[j].d.Density);

				  /* Since the star mFe and mO values are handled with fractions (multiplied by the mass of the star), we
				     don't need to reduce these quantities directly */
#endif
#endif

#else
				  mass_loss = All.MassFeedbackFactor * mass; //change this later
#endif
				  SphP[j].iim.dInjected_W_Mass += FLT(mass_loss * P[j].Mass * wk / SphP[j].d.Density);
#endif
	 			}

			    }
			}
		    }
		}
	    }
	}

      if(mode == 1)
	{
	  listindex++;
	  if(listindex < NODELISTLENGTH)
	    {
	      startnode = StarDataGet[target].NodeList[listindex];
	      if(startnode >= 0)
		startnode = Nodes[startnode].u.d.nextnode;	/* open it */
	    }
	}
    }

  return 0;
}

int ngb_treefind_star(MyDouble searchcenter[3], MyFloat hsml, int target, int *startnode, int mode, int *nexport, int *nsend_local)
{
  int numngb, no, p, task, nexport_save;
  struct NODE *current;
  MyDouble dx, dy, dz, dist;
  
#ifdef PERIODIC
  MyDouble xtmp;
#endif
  
  nexport_save = *nexport;

  numngb = 0;
  no = *startnode;

  while(no >= 0)
    {
      if(no < All.MaxPart)	/* single particle */
	{
	  p = no;
	  no = Nextnode[no];

#ifndef REPOSITION_ON_POTMIN
	  if(P[p].Type != 0 && P[p].Type != 5)
	    continue;
#endif
	  dist = hsml;
	  dx = NGB_PERIODIC_LONG_X(P[p].Pos[0] - searchcenter[0]);
	  if(dx > dist)
	    continue;
	  dy = NGB_PERIODIC_LONG_Y(P[p].Pos[1] - searchcenter[1]);
	  if(dy > dist)
	    continue;
	  dz = NGB_PERIODIC_LONG_Z(P[p].Pos[2] - searchcenter[2]);
	  if(dz > dist)
	    continue;
	  if(dx * dx + dy * dy + dz * dz > dist * dist)
	    continue;

	  Ngblist[numngb++] = p;
	}
      else
	{
	  if(no >= All.MaxPart + MaxNodes)	/* pseudo particle */
	    {	      
	      if(mode == 1)
		endrun(12312);

	      if(Exportflag[task = DomainTask[no - (All.MaxPart + MaxNodes)]] != target)
		{
		  Exportflag[task] = target;
		  Exportnodecount[task] = NODELISTLENGTH;
		}

	      if(Exportnodecount[task] == NODELISTLENGTH)
		{
		  if(*nexport >= All.BunchSize)
		    {
		      *nexport = nexport_save;
		      for(task = 0; task < NTask; task++)
			nsend_local[task] = 0;
		      for(no = 0; no < nexport_save; no++)
			nsend_local[DataIndexTable[no].Task]++;
		      return -1;
		    }
		  Exportnodecount[task] = 0;
		  Exportindex[task] = *nexport;
		  DataIndexTable[*nexport].Task = task;
		  DataIndexTable[*nexport].Index = target;
		  DataIndexTable[*nexport].IndexGet = *nexport;
		  *nexport = *nexport + 1;
		  nsend_local[task]++;
		}

	      DataNodeList[Exportindex[task]].NodeList[Exportnodecount[task]++] =
		DomainNodeIndex[no - (All.MaxPart + MaxNodes)];

	      if(Exportnodecount[task] < NODELISTLENGTH)
		DataNodeList[Exportindex[task]].NodeList[Exportnodecount[task]] = -1;

	      no = Nextnode[no - MaxNodes];
	      continue;
	    }

	  current = &Nodes[no];

	  if(mode == 1)
	    {
	      if(current->u.d.bitflags & (1 << BITFLAG_TOPLEVEL))	/* we reached a top-level node again, which means that we are done with the branch */
		{
		  *startnode = -1;
		  return numngb;
		}
	    }

	  no = current->u.d.sibling;	/* in case the node can be discarded */

	  dist = hsml + 0.5 * current->len;;
	  dx = NGB_PERIODIC_LONG_X(current->center[0] - searchcenter[0]);
	  if(dx > dist)
	    continue;
	  dy = NGB_PERIODIC_LONG_Y(current->center[1] - searchcenter[1]);
	  if(dy > dist)
	    continue;
	  dz = NGB_PERIODIC_LONG_Z(current->center[2] - searchcenter[2]);
	  if(dz > dist)
	    continue;
	  /* now test against the minimal sphere enclosing everything */
	  dist += FACT1 * current->len;
	  if(dx * dx + dy * dy + dz * dz > dist * dist)
	    continue;

	  no = current->u.d.nextnode;	/* ok, we need to open the node */
	}
    }

  *startnode = -1;
  return numngb;
}

#endif

