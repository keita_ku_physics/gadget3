/* last modified: May 23, 2014

 */
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <math.h>
#include <gsl/gsl_math.h>

#include "allvars.h"
#include "proto.h"

#if defined(STELLAR_FEEDBACK) || defined(SNII)

static struct sniimaindata1_in
{
    MyDouble   Pos[3];
    MyFloat    Hsml;
    MyFloat    Mass;
    MyFloat    Density;
    
    int NodeList[NODELISTLENGTH];
}
*SNIIMainData1In, *SNIIMainData1Get;

static struct sniimaindata2_in
{
    MyDouble   Pos[3];
    MyFloat    Hsml;
    MyFloat    Mass;
    MyFloat    Density;
    MyFloat    Metallicity;
    MyFloat    StarNgbMasswk;
    MyFloat    GasNgbMasswk;
    int        StarNgbNum;
    int        GasNgbNum;
#ifdef STELLAR_FEEDBACK
    MyFloat    StellarFB_Energy;
#endif    
    int NodeList[NODELISTLENGTH];
}
*SNIIMainData2In, *SNIIMainData2Get;


static struct sniimaindata_out
{
    //MyFloat Rho; // for BlueWaters
    MyFloat Out;
}
*SNIIMainDataResult, *SNIIMainDataOut;


void snii_main(void)
{
    MyFloat *Left, *Right;
    int i, j, k, ndone, ndone_flag, npleft, dummy, iter = 0;
    int ngrp, sendTask, recvTask, place, nexport, nimport;
    
    double hubble_a, ascale; 
    double time_explosion;
    double time, stellarage;
    
    if(All.ComovingIntegrationOn)
    {
        hubble_a = hubble_function(All.Time);
        ascale = All.Time;
    }
    else
    {
        hubble_a = ascale = 1;
    }
    
    
    Ngblist = (int *) mymalloc(NumPart * sizeof(int));
    
    All.BunchSize =
    (int) ((All.BufferSize * 1024 * 1024) / (sizeof(struct data_index) + sizeof(struct data_nodelist) +
                                             sizeof(struct sniimaindata1_in) + sizeof(struct sniimaindata_out) +
                                             sizemax(sizeof(struct sniimaindata1_in),
                                                     sizeof(struct sniimaindata_out))));
    DataIndexTable = (struct data_index *) mymalloc(All.BunchSize * sizeof(struct data_index));
    DataNodeList = (struct data_nodelist *) mymalloc(All.BunchSize * sizeof(struct data_nodelist));
    
    i = FirstActiveParticle;
    
    do
    {
        for(j = 0; j < NTask; j++)
        {
            Send_count[j] = 0;
            Exportflag[j] = -1;
        }
            
        /* do local particles and prepare export list */
        for(nexport = 0; i >= 0; i = NextActiveParticle[i])
        {

            if(P[i].Type == 4 && P[i].Exploded == 0)
            {
                if(snii_main_evaluate(i, 0, 1, &nexport, Send_count) < 0)
                    break;
            }
        }
            
#ifdef MYSORT
        mysort_dataindex(DataIndexTable, nexport, sizeof(struct data_index), data_index_compare);
#else
        qsort(DataIndexTable, nexport, sizeof(struct data_index), data_index_compare);
#endif
            
        MPI_Allgather(Send_count, NTask, MPI_INT, Sendcount_matrix, NTask, MPI_INT, MPI_COMM_WORLD);
            
        for(j = 0, nimport = 0, Recv_offset[0] = 0, Send_offset[0] = 0; j < NTask; j++)
        {
            Recv_count[j] = Sendcount_matrix[j * NTask + ThisTask];
            nimport += Recv_count[j];
                
            if(j > 0)
            {
                Send_offset[j] = Send_offset[j - 1] + Send_count[j - 1];
                Recv_offset[j] = Recv_offset[j - 1] + Recv_count[j - 1];
            }
        }
            
        SNIIMainData1Get = (struct sniimaindata1_in *) mymalloc(nimport * sizeof(struct sniimaindata1_in));
        SNIIMainData1In = (struct sniimaindata1_in *) mymalloc(nexport * sizeof(struct sniimaindata1_in));
            
        /* prepare particle data for export */
        for(j = 0; j < nexport; j++)
        {
            place = DataIndexTable[j].Index;
                
            SNIIMainData1In[j].Pos[0] = P[place].Pos[0];
            SNIIMainData1In[j].Pos[1] = P[place].Pos[1];
            SNIIMainData1In[j].Pos[2] = P[place].Pos[2];
            SNIIMainData1In[j].Hsml   = P[place].sniiHsml;
                       
            SNIIMainData1In[j].Mass          = P[place].Mass;
            SNIIMainData1In[j].Density       = P[place].sniid.Density;
                
            memcpy(SNIIMainData1In[j].NodeList,
                    DataNodeList[DataIndexTable[j].IndexGet].NodeList, NODELISTLENGTH * sizeof(int));
                
                
            }
            
            /* exchange particle data */
            for(ngrp = 1; ngrp < (1 << PTask); ngrp++)
            {
                sendTask = ThisTask;
                recvTask = ThisTask ^ ngrp;
                
                if(recvTask < NTask)
                {
                    if(Send_count[recvTask] > 0 || Recv_count[recvTask] > 0)
                    {
                        /* get the particles */
                        
#ifdef USE_ISEND_IRECV
                        MPI_Isend(&SNIIMainData1In[Send_offset[recvTask]],
                                  Send_count[recvTask] * sizeof(struct sniimaindata1_in), MPI_BYTE,
                                  recvTask, TAG_DENS_A,MPI_COMM_WORLD, &mpireq[0]);
                        MPI_Irecv(&SNIIMainData1Get[Recv_offset[recvTask]],
                                  Recv_count[recvTask] * sizeof(struct sniimaindata1_in), MPI_BYTE,
                                  recvTask, TAG_DENS_A, MPI_COMM_WORLD, &mpireq[1]);
                        MPI_Waitall(2,mpireq,stat);
#else
                        MPI_Sendrecv(&SNIIMainData1In[Send_offset[recvTask]],
                                     Send_count[recvTask] * sizeof(struct sniimaindata1_in), MPI_BYTE,
                                     recvTask, TAG_DENS_A,
                                     &SNIIMainData1Get[Recv_offset[recvTask]],
                                     Recv_count[recvTask] * sizeof(struct sniimaindata1_in), MPI_BYTE,
                                     recvTask, TAG_DENS_A, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
#endif
                    }
                }
            }
            
            myfree(SNIIMainData1In);
           
            /* now do the particles that were sent to us */
            for(j = 0; j < nimport; j++)
            {
                if(P[j].Type == 4 && P[j].Exploded == 0)
                    snii_main_evaluate(j, 1, 1, &dummy, &dummy);
            }

        
            if(i < 0)
                ndone_flag = 1;
            else
                ndone_flag = 0;
            
            MPI_Allreduce(&ndone_flag, &ndone, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
            
            myfree(SNIIMainData1Get);
        }
        while(ndone < NTask);
        
    
    myfree(DataNodeList);
    myfree(DataIndexTable);
    myfree(Ngblist);
    
    ////// SECOND LOOP ///////////////////////////////
    /////             
    ////
    ///     
    //

    Ngblist = (int *) mymalloc(NumPart * sizeof(int));
    
    All.BunchSize =
    (int) ((All.BufferSize * 1024 * 1024) / (sizeof(struct data_index) + sizeof(struct data_nodelist) +
                                             sizeof(struct sniimaindata2_in) + sizeof(struct sniimaindata_out) +
                                             sizemax(sizeof(struct sniimaindata2_in),
                                                     sizeof(struct sniimaindata_out))));
    DataIndexTable = (struct data_index *) mymalloc(All.BunchSize * sizeof(struct data_index));
    DataNodeList = (struct data_nodelist *) mymalloc(All.BunchSize * sizeof(struct data_nodelist));
    
    i = FirstActiveParticle;
    
    do
    {
        for(j = 0; j < NTask; j++)
        {
            Send_count[j] = 0;
            Exportflag[j] = -1;
        }
        
        /* do local particles and prepare export list */
        for(nexport = 0; i >= 0; i = NextActiveParticle[i])
        {
            if(P[i].Type == 4 && P[i].Exploded == 0)
            {
                if(snii_main_evaluate(i, 0, 2, &nexport, Send_count) < 0)
                    break;
            }
        }
        
#ifdef MYSORT
        mysort_dataindex(DataIndexTable, nexport, sizeof(struct data_index), data_index_compare);
#else
        qsort(DataIndexTable, nexport, sizeof(struct data_index), data_index_compare);
#endif
        
        MPI_Allgather(Send_count, NTask, MPI_INT, Sendcount_matrix, NTask, MPI_INT, MPI_COMM_WORLD);
        
        for(j = 0, nimport = 0, Recv_offset[0] = 0, Send_offset[0] = 0; j < NTask; j++)
        {
            Recv_count[j] = Sendcount_matrix[j * NTask + ThisTask];
            nimport += Recv_count[j];
            
            if(j > 0)
            {
                Send_offset[j] = Send_offset[j - 1] + Send_count[j - 1];
                Recv_offset[j] = Recv_offset[j - 1] + Recv_count[j - 1];
            }
        }
        
        SNIIMainData2Get = (struct sniimaindata2_in *) mymalloc(nimport * sizeof(struct sniimaindata2_in));
        SNIIMainData2In = (struct sniimaindata2_in *) mymalloc(nexport * sizeof(struct sniimaindata2_in));
        
        /* prepare particle data for export */
        for(j = 0; j < nexport; j++)
        {
            place = DataIndexTable[j].Index;
            
            SNIIMainData2In[j].Pos[0] = P[place].Pos[0];
            SNIIMainData2In[j].Pos[1] = P[place].Pos[1];
            SNIIMainData2In[j].Pos[2] = P[place].Pos[2];
            SNIIMainData2In[j].Hsml   = P[place].sniiHsml;
            
            SNIIMainData2In[j].Mass             = P[place].Mass;
            SNIIMainData2In[j].Density          = P[place].sniid.Density;
            SNIIMainData2In[j].Metallicity      = P[place].Metallicity;
            SNIIMainData2In[j].StarNgbMasswk    = P[place].StarNgbMasswk;
            SNIIMainData2In[j].GasNgbMasswk     = P[place].GasNgbMasswk;
            SNIIMainData2In[j].StarNgbNum       = P[place].StarNgbNum;   
            SNIIMainData2In[j].GasNgbNum        = P[place].GasNgbNum;
#ifdef STELLAR_FEEDBACK
            SNIIMainData2In[j].StellarFB_Energy = P[place].StellarFB_Energy;
#endif
            
            memcpy(SNIIMainData2In[j].NodeList,
                   DataNodeList[DataIndexTable[j].IndexGet].NodeList, NODELISTLENGTH * sizeof(int));
            
        }
        
        /* exchange particle data */
        for(ngrp = 1; ngrp < (1 << PTask); ngrp++)
        {
            sendTask = ThisTask;
            recvTask = ThisTask ^ ngrp;
            
            if(recvTask < NTask)
            {
                if(Send_count[recvTask] > 0 || Recv_count[recvTask] > 0)
                {
                    /* get the particles */
                    
#ifdef USE_ISEND_IRECV
                    MPI_Isend(&SNIIMainData2In[Send_offset[recvTask]],
                              Send_count[recvTask] * sizeof(struct sniimaindata2_in), MPI_BYTE,
                              recvTask, TAG_DENS_A,MPI_COMM_WORLD, &mpireq[0]);
                    MPI_Irecv(&SNIIMainData2Get[Recv_offset[recvTask]],
                              Recv_count[recvTask] * sizeof(struct sniimaindata2_in), MPI_BYTE,
                              recvTask, TAG_DENS_A, MPI_COMM_WORLD, &mpireq[1]);
                    MPI_Waitall(2,mpireq,stat);
#else
                    MPI_Sendrecv(&SNIIMainData2In[Send_offset[recvTask]],
                                 Send_count[recvTask] * sizeof(struct sniimaindata2_in), MPI_BYTE,
                                 recvTask, TAG_DENS_A,
                                 &SNIIMainData2Get[Recv_offset[recvTask]],
                                 Recv_count[recvTask] * sizeof(struct sniimaindata2_in), MPI_BYTE,
                                 recvTask, TAG_DENS_A, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
#endif
                }
            }
        }
        
        myfree(SNIIMainData2In);
               
        
        /* now do the particles that were sent to us */
        for(j = 0; j < nimport; j++)
        {
            if(P[j].Type == 4 && P[j].Exploded == 0)
            {
                snii_main_evaluate(j, 1, 2, &dummy, &dummy);
                
            }
        }
        
        if(i < 0)
            ndone_flag = 1;
        else
            ndone_flag = 0;
        
        MPI_Allreduce(&ndone_flag, &ndone, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
        

        myfree(SNIIMainData2Get);
    }
    while(ndone < NTask);
    
    
    myfree(DataNodeList);
    myfree(DataIndexTable);
    myfree(Ngblist);
    
}



int snii_main_evaluate(int target, int mode, int loop, int *nexport, int *nsend_local)
{
    if(P[target].Exploded != 0 || P[target].Type != 4)
    {
        printf("This star has already exploded.\n");
        endrun(1567);
    }
   
    int i, j, k, n;
    int startnode, numngb_inbox, listindex = 0;
    double h, h2, hinv, hinv3, wk;
    double dx, dy, dz, r, r2, u, mass_j, dt;
       
    MyDouble *pos;
    double mass, ngb_den, time_explosion, Z, time, stellarage, deposited_time;

    double mass_return, mZ_produced, dmZ_produced;                    /*   Mass     */
    double snii_energy, sniith_energy, sniik_energy;                 /*   Energy   */
    double Ngb_Den;                                                 /*   Density  */
    double P_cgs, Cs;                                              /*   P & Cs   */
    double sniisngb_masswk, sniigngb_masswk, R_shock;
    short int kicked_flag, heated_flag;
    kicked_flag = heated_flag = 0;
    int vflag, sniisngb, sniigngb, xflag;
    vflag = R_shock = xflag = 0;
    //double actual_returned_mass;
    //actual_returned_mass = 0;

    
    double hubble_a, ascale, a3inv, time_hubble_a;
    double stellarfb_energy, dstellarfb_energy, stellarfb_energy_all;
    
    if(All.ComovingIntegrationOn)
    {
        ascale = All.Time;
        a3inv = 1 / (All.Time * All.Time * All.Time);
        stellarage = a2t(P[target].StellarAge);
        time = a2t(All.Time);
	hubble_a = hubble_function(All.Time);
        time_hubble_a = All.Time * hubble_a;
#ifdef STELLAR_FEEDBACK
        deposited_time = a2t(P[target].DepositedTime);
#endif
    }
    else
    {
        hubble_a = ascale = a3inv = 1;
        stellarage = P[target].StellarAge;
        time = All.Time;
#ifdef STELLAR_FEEDBACK
        deposited_time = P[target].DepositedTime;
#endif
    }
    
    if(mode == 0)
    {
        pos         = P[target].Pos;
        h           = P[target].sniiHsml;
        mass        = P[target].Mass;
        ngb_den     = P[target].sniid.Density;
        if(loop == 2)
        {
            Z                = P[target].Metallicity;
            sniisngb_masswk  = P[target].StarNgbMasswk;
            sniisngb         = P[target].StarNgbNum;  
            sniigngb_masswk  = P[target].GasNgbMasswk;
            sniigngb         = P[target].GasNgbNum;

        }
      
        
    }
    else
    {
        if(loop == 1)
        {
            pos         = SNIIMainData1Get[target].Pos;
            h           = SNIIMainData1Get[target].Hsml;
            mass        = SNIIMainData1Get[target].Mass;
            ngb_den     = SNIIMainData1Get[target].Density;
        }
        else
        {
            pos              = SNIIMainData2Get[target].Pos;
            h                = SNIIMainData2Get[target].Hsml;
            mass             = SNIIMainData2Get[target].Mass;
            ngb_den          = SNIIMainData2Get[target].Density;
            Z                = SNIIMainData2Get[target].Metallicity;
            sniisngb_masswk  = SNIIMainData2Get[target].StarNgbMasswk;
            sniisngb         = SNIIMainData2Get[target].StarNgbNum;
            sniigngb_masswk  = SNIIMainData2Get[target].GasNgbMasswk;
            sniigngb         = SNIIMainData2Get[target].GasNgbNum;

        }
    }
    
    
    h2 = h * h;
    hinv = 1.0 / h;
#ifndef  TWODIMS
    hinv3 = hinv * hinv * hinv;
#else
    hinv3 = hinv * hinv / boxSize_Z;
#endif
    
    if(mode == 0)
    {
        startnode = All.MaxPart;	/* root node */
    }
    else
    {
        if(loop == 1)
            startnode = SNIIMainData1Get[target].NodeList[0];
        else
            startnode = SNIIMainData2Get[target].NodeList[0];
        startnode = Nodes[startnode].u.d.nextnode;	/* open it */
    } 


    if(All.ComovingIntegrationOn)
        ngb_den *= All.HubbleParam * All.HubbleParam * a3inv;
    else
        ngb_den *= a3inv;
    
#ifdef STELLAR_FEEDBACK
    if((time - stellarage) < TIME_TO_EXPLODE)
    {
        if(loop == 1)
            sniigngb_masswk = sniigngb = 0; 
        
        if(loop == 2)
        {
            stellarfb_energy_all = All.StellarFBEfficiency * STELLAR_FB_ENERGY / All.UnitEnergy_in_cgs * mass * All.UnitMass_in_g / SOLAR_MASS;
            dstellarfb_energy    = stellarfb_energy_all / TIME_TO_EXPLODE * (time - deposited_time);
            

	   if(dstellarfb_energy < 0)
		{ 
		printf("SFB: negative value for dstellarfb_energy: %g\n",dstellarfb_energy);
		dstellarfb_energy = 0;
		}

	   if((time - deposited_time)/TIME_TO_EXPLODE > 1.0)
		{
		printf("SFB: illogical activity by the code: %g\n",(time - deposited_time)/TIME_TO_EXPLODE);
		dstellarfb_energy = 0;
		}	


           // printf("\n(time-deposited)/TIME_TO_EXPLODE = %g: %g,%g,%g",(time - deposited_time)/TIME_TO_EXPLODE, TIME_TO_EXPLODE, time, deposited_time );
           // printf("\nAll.Time=%g, All.Time/hubble_a=%g, a2t=%g ", All.Time, All.Time/hubble_a, a2t(All.Time/hubble_a) );
	    if((P[target].StellarFB_Energy - dstellarfb_energy) < 0)
            {
                printf("STELLAR_FEEDBACK: Stop depositing more energy than you should!\n");
                printf("%g, %g\n",P[target].StellarFB_Energy, dstellarfb_energy);
                printf("   ---> The star will deposit all it has left instead.\n");
                //dstellarfb_energy = P[target].StellarFB_Energy; 
                //P[target].StellarFB_Energy = 0;
                dstellarfb_energy = 0;
            }
            
            if(P[target].StellarFB_Energy != stellarfb_energy_all)
            {
                printf("STELLAR_FEEDBACK: Mismatch in available energy!\n");
                printf("P[target].StellarFB_Energy = %g, stellarfb_energy_all = %g \n", P[target].StellarFB_Energy, stellarfb_energy_all);
                //P[target].StellarFB_Energy = 0;
                
            }
                            
            
        
        }
        
    }
    /*
    if(((time - stellarage) >= TIME_TO_EXPLODE) && P[target].StellarFB_Energy > 0)
    {
        if(loop == 2)
        {
            
            //stellarfb_energy_all = All.StellarFBEfficiency * STELLAR_FB_ENERGY / All.UnitEnergy_in_cgs * mass * All.UnitMass_in_g / SOLAR_MASS;
            //dstellarfb_energy = P[target].StellarFB_Energy;
            //printf("\noriginal energy = %0.2f leftover energy= %0.2f \n",stellarfb_energy_all,dstellarfb_energy);
            //printf("leftover percentage=%0.2f\n", (dstellarfb_energy*100/stellarfb_energy_all));
            
            P[target].StellarFB_Energy = 0;
        }
    }
     */
#endif
    
#ifdef SNII
    snii_energy = All.SNIIEfficiency * CHABRIER_IMF2 * mass * All.UnitMass_in_g / SOLAR_MASS;
    Ngb_Den = ngb_den * All.UnitDensity_in_cgs / PROTONMASS;
    R_shock = 7.32e19 * pow(snii_energy, 0.29) * pow(Ngb_Den, -0.42) * 3.240779e-22; // in kpc

    if((time - stellarage) >= TIME_TO_EXPLODE) 
    {
       if(loop == 1)
          sniisngb_masswk = sniisngb = 0;
            
       if(loop == 2)
       {
          sniith_energy = All.SNIIEfficiency * (FIDUCIAL_SNII / All.UnitEnergy_in_cgs) * CHABRIER_IMF2 * THERMAL_E_FRAC * 
                          mass * All.UnitMass_in_g / SOLAR_MASS;
          sniik_energy = All.SNIIEfficiency * CHABRIER_IMF2 * KINETIC_E_FRAC * mass * All.UnitMass_in_g / SOLAR_MASS;
        
          P_cgs = GAMMA_MINUS1 * ngb_den * All.InitGasU * All.UnitMass_in_g / pow(3.08567758e16, 3) / 1.0e5; 
          // g / s^2 cm^-1 //All.UnitMass_in_cgs = 1e10*2*10^33 
        
          Cs = sqrt(GAMMA * P_cgs / (ngb_den * All.UnitDensity_in_cgs)); // in cm
          Cs /= 1e6; //in 10 km/s 
          
          mass_return = RETURN_FRAC_SNII * mass;    
          mZ_produced = (2.09 * FRAC_O + 1.06 * FRAC_Fe) * mass_return; // AGORA: Kim et al. 2013
       }
    } 
#endif
    
    while(startnode >= 0)
    {
        while(startnode >= 0)
        {
            numngb_inbox = ngb_treefind_snii(pos, h, target, &startnode, mode, nexport, nsend_local);
            
            if(numngb_inbox < 0)
                return -1;
            
              for(n = 0; n < numngb_inbox; n++)
            {
                j = Ngblist[n];

                dx = pos[0] - P[j].Pos[0];
                dy = pos[1] - P[j].Pos[1];
                dz = pos[2] - P[j].Pos[2];
                
#ifdef PERIODIC		/*  now find the closest image in the given box size  */
                if(dx > boxHalf_X)
                    dx -= boxSize_X;
                if(dx < -boxHalf_X)
                    dx += boxSize_X;
                if(dy > boxHalf_Y)
                    dy -= boxSize_Y;
                if(dy < -boxHalf_Y)
                    dy += boxSize_Y;
                if(dz > boxHalf_Z)
                    dz -= boxSize_Z;
                if(dz < -boxHalf_Z)
                    dz += boxSize_Z;
#endif
                r2 = dx * dx + dy * dy + dz * dz;
                
                if(r2 < h2)
                {
                    r = sqrt(r2);
                    u = r * hinv;
          
                    if(u < 0.5)
                        wk = hinv3 * (KERNEL_COEFF_1 + KERNEL_COEFF_2 * (u - 1) * u * u);
                    else
                        wk = hinv3 * KERNEL_COEFF_5 * (1.0 - u) * (1.0 - u) * (1.0 - u);
                        
                    mass_j = P[j].Mass;
                    
#ifdef STELLAR_FEEDBACK
                    if((time - stellarage) < TIME_TO_EXPLODE)
                    {
                        if(loop == 1)
                        {
                            sniigngb_masswk += FLT(mass_j * wk);
                            sniigngb++;
                        }
                        
                        if(loop == 2 && heated_flag == 0)
                        {    
                        if(sniigngb != 0)
                        {
                            SphP[j].sniis.dEnergy = FLT(dstellarfb_energy * mass_j * wk / sniigngb_masswk);
                            xflag++; 
                            
                            if(xflag >= sniigngb) 
                                heated_flag++;
                        }
                        }
                    }

#endif
                    
#ifdef SNII
                    if((time - stellarage) >= TIME_TO_EXPLODE) 
                    {
                        if(loop == 1)
                        {
                           if(r <= R_shock)
                           {
                              sniisngb_masswk += FLT(mass_j * wk);
                              sniisngb++;
                           }
                        }
                    
                        if(loop == 2 && kicked_flag == 0)
                        {
                        if(sniisngb != 0 && r <= R_shock)
                        {
                            SphP[j].sniik.dKinetic_Energy = FLT(sniik_energy * mass_j * wk / sniisngb_masswk);
                            SphP[j].sniith.dThermal_Energy = FLT(sniith_energy * mass_j * wk / sniisngb_masswk);
                            /*
                            Ek  += SphP[j].sniik.dKinetic_Energy;
                            Eth += SphP[j].sniith.dThermal_Energy;
                            */
                            SphP[j].StarNgbMass = mass;
                            SphP[j].StarNgbDensity = Ngb_Den;
                            SphP[j].StarNgbSoundSpd = Cs;
                            SphP[j].Rshock = R_shock;
                            P[j].StarNgbNum = sniisngb;
                            vflag++;
                            
                            
                            P[j].MassReturn = FLT(mass_return * mass_j * wk / sniisngb_masswk);
                            //actual_returned_mass += P[j].MassReturn;
                            dmZ_produced = FLT(mZ_produced * mass_j * wk / sniisngb_masswk);
                            SphP[j].sniiz.MassZ = dmZ_produced + Z * mass_return;
                             
                            if(vflag >= sniisngb)
                                kicked_flag++;     
                            
                            if(kicked_flag > 0)
                                P[target].Exploded = 1;
                            
                        }
                        } // end of loop = 2
                    }
#endif
                }
            }// end of a for-loop for ngb_inbox
           
        }
        
        if(mode == 1)
        {
            listindex++;
            if(listindex < NODELISTLENGTH)
            {
                if(loop == 1)
                    startnode = SNIIMainData1Get[target].NodeList[listindex];
                else
                    startnode = SNIIMainData2Get[target].NodeList[listindex];
                
                if(startnode >= 0)
                    startnode = Nodes[startnode].u.d.nextnode;	/* open it */
            }
        }
    } 

    
    if(loop == 1)
    {
        P[target].StarNgbMasswk = sniisngb_masswk;
        P[target].StarNgbNum    = sniisngb;
        P[target].GasNgbMasswk  = sniigngb_masswk;
        P[target].GasNgbNum     = sniigngb;
    }
    
    if(loop == 2)
    {
#ifdef STELLAR_FEEDBACK
        if((time - stellarage) < TIME_TO_EXPLODE)
        {
            P[target].DepositedTime = All.Time;
            P[target].StellarFB_Energy -= dstellarfb_energy;
        }
#endif
        
        if((time - stellarage) >= TIME_TO_EXPLODE)
        {
#ifndef SNII  // Mark the star as exploded when only SFB is used           
            P[target].Exploded = 1;
#endif
#ifdef STELLAR_FEEDBACK
            P[target].StellarFB_Energy = 0;
            P[target].DepositedTime = 0;
#endif
        }
        
#ifdef SNII
        /*
        // TESTING 
        Ethact = sniith_energy;
        Ekact  = sniik_energy;
        printf("*** Energy must be conserved ***\n***   ****   ***\n");
        printf("*** Distributed Eth&Ek : %g & %g \n", Eth, Ek);
        printf("*** Actual E available: %g & %g \n*** *** ***\n", Ethact, Ekact);
        //
        */
        //printf("*** Mass & Z must be conserved ***\n***   ****   ***\n");
        //printf("*** Retunred mass has to be %g : Actual returned mass = %g\n", mass_return, actual_returned_mass);
        //printf("*** returned mass - star's returned mass= %g\n *** *** ***\n", mass_return - P[target].MassReturn);
        if((time - stellarage) >= TIME_TO_EXPLODE)
            P[target].MassReturn -= mass_return;
        
#endif
    }
    
    return 0;
}


int ngb_treefind_snii(MyDouble searchcenter[3], MyFloat hsml, int target, int *startnode, int mode,
                        int *nexport, int *nsend_local)
{
    int numngb, no, p, task, nexport_save;
    struct NODE *current;
    MyDouble dx, dy, dz, dist;
    
#ifdef PERIODIC
    MyDouble xtmp;
#endif
    nexport_save = *nexport;
    
    numngb = 0;
    no = *startnode;
    
    while(no >= 0)
    {
        if(no < All.MaxPart)	/* single particle */
        {
            p = no;
            no = Nextnode[no];
            
            if(P[p].Type != 0) 
                continue;
            
            if(P[p].Ti_current != All.Ti_Current)
                drift_particle(p, All.Ti_Current);
            
            dist = hsml;
#ifdef PERIODIC
            dx = NGB_PERIODIC_LONG_X(P[p].Pos[0] - searchcenter[0]);
            if(dx > dist)
                continue;
            dy = NGB_PERIODIC_LONG_Y(P[p].Pos[1] - searchcenter[1]);
            if(dy > dist)
                continue;
            dz = NGB_PERIODIC_LONG_Z(P[p].Pos[2] - searchcenter[2]);
            if(dz > dist)
                continue;
#else
            dx = P[p].Pos[0] - searchcenter[0];
            if(dx > dist)
                continue;
            dy = P[p].Pos[1] - searchcenter[1];
            if(dy > dist)
                continue;
            dz = P[p].Pos[2] - searchcenter[2];
            if(dz > dist)
                continue;
#endif
            if(dx * dx + dy * dy + dz * dz > dist * dist)
                continue;
            
            Ngblist[numngb++] = p;
        }
        else
        {
            if(no >= All.MaxPart + MaxNodes)	/* pseudo particle */
            {
                if(mode == 1)
                    endrun(12312);
                
                if(target >= 0)	/* if no target is given, export will not occur */
                {
                    if(Exportflag[task = DomainTask[no - (All.MaxPart + MaxNodes)]] != target)
                    {
                        Exportflag[task] = target;
                        Exportnodecount[task] = NODELISTLENGTH;
                    }
                    
                    if(Exportnodecount[task] == NODELISTLENGTH)
                    {
                        if(*nexport >= All.BunchSize)
                        {
                            *nexport = nexport_save;
                            if(nexport_save == 0)
                                endrun(13004);	/* in this case, the buffer is too small to process even a single particle */
                            for(task = 0; task < NTask; task++)
                                nsend_local[task] = 0;
                            for(no = 0; no < nexport_save; no++)
                                nsend_local[DataIndexTable[no].Task]++;
                            return -1;
                        }
                        Exportnodecount[task] = 0;
                        Exportindex[task] = *nexport;
                        DataIndexTable[*nexport].Task = task;
                        DataIndexTable[*nexport].Index = target;
                        DataIndexTable[*nexport].IndexGet = *nexport;
                        *nexport = *nexport + 1;
                        nsend_local[task]++;
                    }
                    
                    DataNodeList[Exportindex[task]].NodeList[Exportnodecount[task]++] =
                    DomainNodeIndex[no - (All.MaxPart + MaxNodes)];
                    
                    if(Exportnodecount[task] < NODELISTLENGTH)
                        DataNodeList[Exportindex[task]].NodeList[Exportnodecount[task]] = -1;
                }
                
                no = Nextnode[no - MaxNodes];
                continue;
            }
            
            current = &Nodes[no];
            
            if(mode == 1)
            {
                if(current->u.d.bitflags & (1 << BITFLAG_TOPLEVEL))	/* we reached a top-level node again, which means that we are done with the branch */
                {
                    *startnode = -1;
                    return numngb;
                }
            }
            
            if(current->Ti_current != All.Ti_Current)
                force_drift_node(no, All.Ti_Current);
            
            no = current->u.d.sibling;	/* in case the node can be discarded */
            
            dist = hsml + 0.5 * current->len;;
#ifdef PERIODIC	  
            dx = NGB_PERIODIC_LONG_X(current->center[0] - searchcenter[0]);
            if(dx > dist)
                continue;
            dy = NGB_PERIODIC_LONG_Y(current->center[1] - searchcenter[1]);
            if(dy > dist)
                continue;
            dz = NGB_PERIODIC_LONG_Z(current->center[2] - searchcenter[2]);
            if(dz > dist)
                continue;
#else
            dx = current->center[0] - searchcenter[0];
            if(dx > dist)
                continue;
            dy = current->center[1] - searchcenter[1];
            if(dy > dist)
                continue;
            dz = current->center[2] - searchcenter[2];
            if(dz > dist)
                continue;
#endif
            /* now test against the minimal sphere enclosing everything */
            dist += FACT1 * current->len;
            if(dx * dx + dy * dy + dz * dz > dist * dist)
                continue;
            
            no = current->u.d.nextnode;	/* ok, we need to open the node */
        }
    }
    
    *startnode = -1;
    return numngb;
}

/* 
 This analytic formula of age combines the analytic formula given 
 in eq.(13.20) in Peebles' book p.317, and the conversion from 
 sinh^(-1) to log(x+sqrt(1+x*x)).  It returns the age of the universe
 for a given value of scale factor.
 Note that this formula works for only flat Lambda universe.
 (from Dr. K.Nagamine's work)
 */
double a2t(double scalefactor)
{
    double factor, ratio, x, age;
    
    factor = (2./3.) * (HUBBLETIME/All.HubbleParam) / sqrt(All.OmegaLambda);
    ratio = All.OmegaLambda / All.Omega0;
    x = sqrt(ratio * pow(scalefactor, 3));
    age = factor * log(x + sqrt(1. + x*x)) * TIME_GYR; // return in Gyr
    return age;
}

#endif




